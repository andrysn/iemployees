<?php

namespace App\Http\Controllers;

use Auth;
use Session;

use App\Models\Keluarga;
use App\Models\Permission;
use Illuminate\Http\Request;

class KeluargaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $modul_id = 23;
        $cek = Permission::where('modul_id', $modul_id)
                ->where('group_id', Auth::user()->group_id)->first();

        if ( $cek != "" ) {
            if ( $modul_id != $cek->modul_id) {
                return "<script language='javascript'>
                        alert('Anda Tidak Memiliki Akses');
                        document.location='".asset('/')."';
                        </script>";
            }
        } else {
            return "<script language='javascript'>
                    alert('Anda Tidak Memiliki Akses');
                    document.location='".asset('/')."';
                    </script>";
        }
        
        $this->validate($request, [
            'tanggal_lahir' => 'required',
            'tempat_lahir' => 'required',
            'hubungan_keluarga' => 'required',
            'nama_lengkap' => 'required',
            'agama' => 'required',
            'nik' => 'nik',
            'jenis_kelamin' => 'required'
        ]);

        $keluarga = Keluarga::firstOrCreate([
            'tanggal_lahir' => $request->tanggal_lahir,
            'tempat_lahir' => $request->tempat_lahir,
            'nama_lengkap' => $request->nama_lengkap,
            'agama' => $request->agama,
            'pekerjaan' => $request->pekerjaan,
            'nik' => $request->nik,
            'jenis_kelamin' => $request->jenis_kelamin,
            'hubungan_keluarga' => $request->hubungan_keluarga,
            'pekerja_id' => $request->pekerja_id
        ]);
        
        
        Session::flash("flash_notif", [
            "level"     => "dismissible alert-success",
            "massage"   => "Berhasil, menyimpan data keluarga" . $keluarga->pekerja->nama_pegawai
        ]);

        return redirect('keluarga/'. $request->pekerja_id);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Keluarga  $keluarga
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $modul_id = 24;
        $cek = Permission::where('modul_id', $modul_id)
                ->where('group_id', Auth::user()->group_id)->first();

        if ( $cek != "" ) {
            if ( $modul_id != $cek->modul_id) {
                return "<script language='javascript'>
                        alert('Anda Tidak Memiliki Akses');
                        document.location='".asset('/')."';
                        </script>";
            }
        } else {
            return "<script language='javascript'>
                    alert('Anda Tidak Memiliki Akses');
                    document.location='".asset('/')."';
                    </script>";
        }
        
        $title = 'Sistem Informasi Pegawai Dinas Perumahan dan Permukiman Samarinda';
        $route = 'keluarga';
        $button = 'Tambah';

        $keluarga = Keluarga::where('pekerja_id' , $id)->orderby('id','desc')->paginate(50);
        return view('keluarga.index', compact('title', 'title', 'route', 'button', 'keluarga'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Keluarga  $keluarga
     * @return \Illuminate\Http\Response
     */
    public function edit(Keluarga $keluarga)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Keluarga  $keluarga
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Keluarga $keluarga)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Keluarga  $keluarga
     * @return \Illuminate\Http\Response
     */
    public function destroy(Keluarga $keluarga)
    {
        $modul_id = 25;
        $cek = Permission::where('modul_id', $modul_id)
                ->where('group_id', Auth::user()->group_id)->first();

        if ( $cek != "" ) {
            if ( $modul_id != $cek->modul_id) {
                return "<script language='javascript'>
                        alert('Anda Tidak Memiliki Akses');
                        document.location='".asset('/')."';
                        </script>";
            }
        } else {
            return "<script language='javascript'>
                    alert('Anda Tidak Memiliki Akses');
                    document.location='".asset('/')."';
                    </script>";
        }
        
        $data = $keluarga;
        $data->delete();
        Session::flash(
            "flash_notif",[
                "level"   => "dismissible alert-info",
                "massage" => "Berhasil Mengapus Data Keluarga <strong>" . $data->pekerja->nama_pegawai ."</strong>"
        ]);
        
        return redirect('keluarga/'. $data->pekerja_id);
    }
}
