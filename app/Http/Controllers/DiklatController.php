<?php

namespace App\Http\Controllers;

use Auth;
use Session;

use App\Models\Diklat;
use App\Models\Permission;
use Illuminate\Http\Request;

class DiklatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $modul_id = 11;
        $cek = Permission::where('modul_id', $modul_id)
                ->where('group_id', Auth::user()->group_id)->first();

        if ( $cek != "" ) {
            if ( $modul_id != $cek->modul_id) {
                return "<script language='javascript'>
                        alert('Anda Tidak Memiliki Akses');
                        document.location='".asset('/')."';
                        </script>";
            }
        } else {
            return "<script language='javascript'>
                    alert('Anda Tidak Memiliki Akses');
                    document.location='".asset('/')."';
                    </script>";
        }
        
        $this->validate($request, [
            'foto_diklat' => 'required|image|mimes:jpeg,jpg,png',
            'tanggal_diklat' => 'required',
            'jenis_diklat' => 'required',
            'nama_diklat' => 'required',
            'penyelenggara' => 'required'
        ]);

        if ($request->hasFile('foto_diklat')) {
            $gambar = $request->file('foto_diklat');
            
            $ext = $gambar->getClientOriginalExtension();
            
            if ($request->file('foto_diklat')->isValid()) {
                $foto_name = "diklat_" . $request->nomor_diklat . date('YmdHis') . ".$ext";
                $path = 'asset/foto-diklat/';
                $request->file('foto_diklat')->move($path, $foto_name);
            }
        }

        $diklat = Diklat::firstOrCreate([
            'tanggal_diklat' => $request->tanggal_diklat,
            'jenis_diklat' => $request->jenis_diklat,
            'foto_diklat' => $foto_name,
            'nama_diklat' => $request->nama_diklat,
            'penyelenggara' => $request->penyelenggara,
            'pekerja_id' => $request->pekerja_id
        ]);
        
        
        Session::flash("flash_notif", [
            "level"     => "dismissible alert-success",
            "massage"   => "Berhasil, menyimpan data pendidikan pegawai"
        ]);

        return redirect('diklat/'. $request->pekerja_id);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Diklat  $diklat
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $modul_id = 12;
        $cek = Permission::where('modul_id', $modul_id)
                ->where('group_id', Auth::user()->group_id)->first();

        if ( $cek != "" ) {
            if ( $modul_id != $cek->modul_id) {
                return "<script language='javascript'>
                        alert('Anda Tidak Memiliki Akses');
                        document.location='".asset('/')."';
                        </script>";
            }
        } else {
            return "<script language='javascript'>
                    alert('Anda Tidak Memiliki Akses');
                    document.location='".asset('/')."';
                    </script>";
        }
        
        $title = 'Sistem Informasi Pegawai Dinas Perumahan dan Permukiman Samarinda';
        $route = 'diklat';
        $button = 'Tambah';

        $diklat = Diklat::where('pekerja_id' , $id)->orderby('id','desc')->paginate(50);
        return view('diklat.index', compact('title', 'title', 'route', 'button', 'diklat'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Diklat  $diklat
     * @return \Illuminate\Http\Response
     */
    public function edit(Diklat $diklat)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Diklat  $diklat
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Diklat $diklat)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Diklat  $diklat
     * @return \Illuminate\Http\Response
     */
    public function destroy(Diklat $diklat)
    {
        $modul_id = 13;
        $cek = Permission::where('modul_id', $modul_id)
                ->where('group_id', Auth::user()->group_id)->first();

        if ( $cek != "" ) {
            if ( $modul_id != $cek->modul_id) {
                return "<script language='javascript'>
                        alert('Anda Tidak Memiliki Akses');
                        document.location='".asset('/')."';
                        </script>";
            }
        } else {
            return "<script language='javascript'>
                    alert('Anda Tidak Memiliki Akses');
                    document.location='".asset('/')."';
                    </script>";
        }
        
        $data = $diklat;
        $data->delete();
        Session::flash(
            "flash_notif",[
                "level"   => "dismissible alert-info",
                "massage" => "Berhasil Mengapus Data Diklat <strong>" . $data->pekerja->nama_pegawai .", Jenjang $data->nama_diklat</strong>"
        ]);
        
        return redirect('diklat/'. $data->pekerja_id);
    }
}
