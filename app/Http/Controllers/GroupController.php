<?php

namespace App\Http\Controllers;

use Auth;
use Session;

use App\Models\Modul;
use App\Models\Group;
use App\Models\Permission;
use Illuminate\Http\Request;

class GroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $modul_id = 14;
        $cek = Permission::where('modul_id', $modul_id)
                ->where('group_id', Auth::user()->group_id)->first();

        if ( $cek != "" ) {
            if ( $modul_id != $cek->modul_id) {
                return "<script language='javascript'>
                        alert('Anda Tidak Memiliki Akses');
                        document.location='".asset('/')."';
                        </script>";
            }
        } else {
            return "<script language='javascript'>
                    alert('Anda Tidak Memiliki Akses');
                    document.location='".asset('/')."';
                    </script>";
        }

        $title = 'Group';
        $route = 'group';
        $group = Group::orderby('nama_group', 'asc')->paginate(10);
        
        return view('group.index', compact('title', 'group', 'route'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $modul_id = 15;
        $cek = Permission::where('modul_id', $modul_id)
                ->where('group_id', Auth::user()->group_id)->first();

        if ( $cek != "" ) {
            if ( $modul_id != $cek->modul_id) {
                return "<script language='javascript'>
                        alert('Anda Tidak Memiliki Akses');
                        document.location='".asset('/')."';
                        </script>";
            }
        } else {
            return "<script language='javascript'>
                    alert('Anda Tidak Memiliki Akses');
                    document.location='".asset('/')."';
                    </script>";
        }

        $title  = 'Tambah Group';
        $button = 'Tambah';
        $route  = 'group';

        $modul = Modul::orderby('nama_modul', 'asc')->get();
        
        return view('template.create', compact('title', 'button', 'route', 'controller', 'modul'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $modul_id = 15;
        $cek = Permission::where('modul_id', $modul_id)
                ->where('group_id', Auth::user()->group_id)->first();

        if ( $cek != "" ) {
            if ( $modul_id != $cek->modul_id) {
                return "<script language='javascript'>
                        alert('Anda Tidak Memiliki Akses');
                        document.location='".asset('/')."';
                        </script>";
            }
        } else {
            return "<script language='javascript'>
                    alert('Anda Tidak Memiliki Akses');
                    document.location='".asset('/')."';
                    </script>";
        }

        $this->validate($request, [
            'nama_group' => 'required'
        ]);
        
        $group =  Group::firstOrCreate([
            'nama_group' => $request->nama_group
        ]);
        
        if ( !empty($request->akses) ) {

            foreach ($request->akses as $akses) {
                Permission::firstOrCreate([
                    'group_id' => $group->id,
                    'modul_id' => $akses
                ]);

            }

        }
        
        Session::flash("flash_notif", [
            "level"     => "dismissible alert-success",
            "message"   => "Berhasil, menyimpan data group <strong>$request->nama_group</strong>"
        ]);

        return \Redirect::route('group.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Group $group)
    {
        $modul_id = 75;
        $cek = Permission::where('modul_id', $modul_id)
                ->where('group_id', Auth::user()->group_id)->first();

        if ( $cek != "" ) {
            if ( $modul_id != $cek->modul_id) {
                return "<script language='javascript'>
                        alert('Anda Tidak Memiliki Akses');
                        document.location='".asset('/')."';
                        </script>";
            }
        } else {
            return "<script language='javascript'>
                    alert('Anda Tidak Memiliki Akses');
                    document.location='".asset('/')."';
                    </script>";
        }

        $modul = Permission::where('group_id', Auth::user()->group_id)->get();
        $title = 'Sistem Informasi Pegawai Dinas Perumahan dan Permukiman Samarinda';
        $route = 'group';
        
        return view('group.show', compact('title', 'route', 'modul'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $modul_id = 16;
        $cek = Permission::where('modul_id', $modul_id)
                ->where('group_id', Auth::user()->group_id)->first();

        if ( $cek != "" ) {
            if ( $modul_id != $cek->modul_id) {
                return "<script language='javascript'>
                        alert('Anda Tidak Memiliki Akses');
                        document.location='".asset('/')."';
                        </script>";
            }
        } else {
            return "<script language='javascript'>
                    alert('Anda Tidak Memiliki Akses');
                    document.location='".asset('/')."';
                    </script>";
        }

        $title  = 'Ubah Group';
        $button = 'Perbarui';
        $route  = 'group';

        $group = Group::findOrFail($id);

        $modul = Modul::orderby('nama_modul', 'asc')->get();
        
            $cek = Permission::where('group_id', Auth::user()->group_id)
            ->whereExists(function($query) {
                $query->selectRaw('moduls.id')
                      ->from('moduls')
                      ->whereRaw('moduls.id = permissions.modul_id');
            })
            ->get();
            
        return view('group.edit', compact('title', 'button', 'route', 'controller', 'cek', 'group', 'modul'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $modul_id = 16;
        // $cek = Permission::where('modul_id', $modul_id)
        //         ->where('group_id', Auth::user()->group_id)->first();

        // if ( $cek != "" ) {
        //     if ( $modul_id != $cek->modul_id) {
        //         return "<script language='javascript'>
        //                 alert('Anda Tidak Memiliki Akses');
        //                 document.location='".asset('/')."';
        //                 </script>";
        //     }
        // } else {
        //     return "<script language='javascript'>
        //             alert('Anda Tidak Memiliki Akses');
        //             document.location='".asset('/')."';
        //             </script>";
        // }

        $this->validate($request, [
            'nama_group' => 'required|unique:groups,nama_group,'.$id,
        ]);

        $group = Group::findOrFail($id);
        $group->nama_group = $request->nama_group;
        $group->update();

        $data = Permission::where('group_id', $id)->delete();

        if ( !empty($request->akses) ) {

            foreach ($request->akses as $akses) {
                Permission::firstOrCreate([
                    'group_id' => $id,
                    'modul_id' => $akses
                ]);

            }

        }
        
        Session::flash(
            "flash_notif", [
                "level"     => "dismissible alert-info",
                "message"   => "Berhasil, memperbarui data group <strong>$request->nama_group</strong>"
            ]);
        
        return \Redirect::route('group.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $modul_id = 17;
        $cek = Permission::where('modul_id', $modul_id)
                ->where('group_id', Auth::user()->group_id)->first();

        if ( $cek != "" ) {
            if ( $modul_id != $cek->modul_id) {
                return "<script language='javascript'>
                        alert('Anda Tidak Memiliki Akses');
                        document.location='".asset('/')."';
                        </script>";
            }
        } else {
            return "<script language='javascript'>
                    alert('Anda Tidak Memiliki Akses');
                    document.location='".asset('/')."';
                    </script>";
        }

        $data = Group::findOrFail($id);
        $data->delete();
        Session::flash(
            "flash_notif",[
                "level"   => "dismissible alert-info",
                "massage" => "Berhasil Mengapus Data <strong>$data->nama_group</strong>"
            ]);
        
        return \Redirect::route('group.index');
    }
}
