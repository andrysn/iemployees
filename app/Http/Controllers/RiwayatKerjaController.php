<?php

namespace App\Http\Controllers;

use Auth;
use Session;

use App\Models\Permission;
use App\Models\RiwayatKerja;
use Illuminate\Http\Request;

class RiwayatKerjaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $modul_id = 53;
        $cek = Permission::where('modul_id', $modul_id)
                ->where('group_id', Auth::user()->group_id)->first();

        if ( $cek != "" ) {
            if ( $modul_id != $cek->modul_id) {
                return "<script language='javascript'>
                        alert('Anda Tidak Memiliki Akses');
                        document.location='".asset('/')."';
                        </script>";
            }
        } else {
            return "<script language='javascript'>
                    alert('Anda Tidak Memiliki Akses');
                    document.location='".asset('/')."';
                    </script>";
        }
        
        $this->validate($request, [
            'tanggal_keluar' => 'required',
            'tanggal_masuk' => 'required',
            'jabatan' => 'required',
            'nama_kantor' => 'required'
        ]);

        $riwayatKerja = RiwayatKerja::firstOrCreate([
            'tanggal_masuk' => $request->tanggal_masuk,
            'tanggal_keluar' => $request->tanggal_keluar,
            'jabatan' => $request->jabatan,
            'nama_kantor' => $request->nama_kantor,
            'keterangan' => $request->keterangan,
            'pekerja_id' => $request->pekerja_id
        ]);
        
        
        Session::flash("flash_notif", [
            "level"     => "dismissible alert-success",
            "massage"   => "Berhasil, menyimpan data pendidikan pegawai"
        ]);

        return redirect('riwayat-kerja/'. $request->pekerja_id);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\RiwayatKerja  $riwayatKerja
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $modul_id = 54;
        $cek = Permission::where('modul_id', $modul_id)
                ->where('group_id', Auth::user()->group_id)->first();

        if ( $cek != "" ) {
            if ( $modul_id != $cek->modul_id) {
                return "<script language='javascript'>
                        alert('Anda Tidak Memiliki Akses');
                        document.location='".asset('/')."';
                        </script>";
            }
        } else {
            return "<script language='javascript'>
                    alert('Anda Tidak Memiliki Akses');
                    document.location='".asset('/')."';
                    </script>";
        }
        
        $title = 'Sistem Informasi Pegawai Dinas Perumahan dan Permukiman Samarinda';
        $route = 'riwayat-kerja';
        $button = 'Tambah';

        $riwayatKerja = RiwayatKerja::where('pekerja_id' , $id)->orderby('id','desc')->paginate(50);
        return view('riwayat-kerja.index', compact('title', 'title', 'route', 'button', 'riwayatKerja'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\RiwayatKerja  $riwayatKerja
     * @return \Illuminate\Http\Response
     */
    public function edit(RiwayatKerja $riwayatKerja)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\RiwayatKerja  $riwayatKerja
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RiwayatKerja $riwayatKerja)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\RiwayatKerja  $riwayatKerja
     * @return \Illuminate\Http\Response
     */
    public function destroy(RiwayatKerja $riwayatKerja)
    {
        $modul_id = 55;
        $cek = Permission::where('modul_id', $modul_id)
                ->where('group_id', Auth::user()->group_id)->first();

        if ( $cek != "" ) {
            if ( $modul_id != $cek->modul_id) {
                return "<script language='javascript'>
                        alert('Anda Tidak Memiliki Akses');
                        document.location='".asset('/')."';
                        </script>";
            }
        } else {
            return "<script language='javascript'>
                    alert('Anda Tidak Memiliki Akses');
                    document.location='".asset('/')."';
                    </script>";
        }
        
        $data = $riwayatKerja;
        $data->delete();
        Session::flash(
            "flash_notif",[
                "level"   => "dismissible alert-info",
                "massage" => "Berhasil Mengapus Data Riwayat Kerja <strong>" . $data->pekerja->nama_pegawai ."</strong>"
        ]);
        
        return redirect('riwayat-kerja/'. $data->pekerja_id);
    }
}
