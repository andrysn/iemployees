<?php

namespace App\Http\Controllers;

use Auth;
use Session;

use App\Models\User;
use App\Models\Permission;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $modul_id = 71;
        $cek = Permission::where('modul_id', $modul_id)
                ->where('group_id', Auth::user()->group_id)->first();

        // if ( $cek != "" ) {
        //     if ( $modul_id != $cek->modul_id) {
        //         return "<script language='javascript'>
        //                 alert('Anda Tidak Memiliki Akses');
        //                 document.location='".asset('/')."';
        //                 </script>";
        //     }
        // } else {
        //     return "<script language='javascript'>
        //             alert('Anda Tidak Memiliki Akses');
        //             document.location='".asset('/')."';
        //             </script>";
        // }

        $title = 'Ganti Password User' ;
        $route = 'user';
        $button = 'Ganti';
        
//        $merek = Merek::orderby('kode_merek', 'asc')->paginate(20);
        
        return view('template.create', compact('title', 'route', 'button'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $modul_id = 72;
        $cek = Permission::where('modul_id', $modul_id)
                ->where('group_id', Auth::user()->group_id)->first();

        if ( $cek != "" ) {
            if ( $modul_id != $cek->modul_id) {
                return "<script language='javascript'>
                        alert('Anda Tidak Memiliki Akses');
                        document.location='".asset('/')."';
                        </script>";
            }
        } else {
            return "<script language='javascript'>
                    alert('Anda Tidak Memiliki Akses');
                    document.location='".asset('/')."';
                    </script>";
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $modul_id = 72;
        // $cek = Permission::where('modul_id', $modul_id)
        //         ->where('group_id', Auth::user()->group_id)->first();

        // if ( $cek != "" ) {
        //     if ( $modul_id != $cek->modul_id) {
        //         return "<script language='javascript'>
        //                 alert('Anda Tidak Memiliki Akses');
        //                 document.location='".asset('/')."';
        //                 </script>";
        //     }
        // } else {
        //     return "<script language='javascript'>
        //             alert('Anda Tidak Memiliki Akses');
        //             document.location='".asset('/')."';
        //             </script>";
        // }

        $this->validate($request, [
            'password_baru' => 'required',
            'password_konfirmasi' => 'required'
        ]);
        
        if ($request->password_baru == $request->password_konfirmasi) {
            
            $password = User::find(1);
                
                $password->password = bcrypt($request->password_baru);
                $password->save();
                
                Session::flash(
                    "flash_notif", [
                        "level"     => "dismissible alert-success",
                        "message"   => "Berhasil, memperbarui data merek Password Berhasil Di Update"
                    ]);
                
                return redirect('user'); 
            
        } else {
            Session::flash(
                "flash_notif", [
                    "level"     => "dismissible alert-danger",
                    "message"   => "Ulangi Konfirmasi Password tidak sama dengan Password Baru"
                ]);
            
            return redirect('user'); 
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $modul_id = 73;
        // $cek = Permission::where('modul_id', $modul_id)
        //         ->where('group_id', Auth::user()->group_id)->first();

        // if ( $cek != "" ) {
        //     if ( $modul_id != $cek->modul_id) {
        //         return "<script language='javascript'>
        //                 alert('Anda Tidak Memiliki Akses');
        //                 document.location='".asset('/')."';
        //                 </script>";
        //     }
        // } else {
        //     return "<script language='javascript'>
        //             alert('Anda Tidak Memiliki Akses');
        //             document.location='".asset('/')."';
        //             </script>";
        // }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $modul_id = 74;
        $cek = Permission::where('modul_id', $modul_id)
                ->where('group_id', Auth::user()->group_id)->first();

        if ( $cek != "" ) {
            if ( $modul_id != $cek->modul_id) {
                return "<script language='javascript'>
                        alert('Anda Tidak Memiliki Akses');
                        document.location='".asset('/')."';
                        </script>";
            }
        } else {
            return "<script language='javascript'>
                    alert('Anda Tidak Memiliki Akses');
                    document.location='".asset('/')."';
                    </script>";
        }
    }
}
