<?php

namespace App\Http\Controllers;

use Auth;
use Session;

use App\Model\Promosi;
use App\Models\Permission;
use Illuminate\Http\Request;

class PromosiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $modul_id = 75;
        $cek = Permission::where('modul_id', $modul_id)
                ->where('group_id', Auth::user()->group_id)->first();

        if ( $cek != "" ) {
            if ( $modul_id != $cek->modul_id) {
                return "<script language='javascript'>
                        alert('Anda Tidak Memiliki Akses');
                        document.location='".asset('/')."';
                        </script>";
            }
        } else {
            return "<script language='javascript'>
                    alert('Anda Tidak Memiliki Akses');
                    document.location='".asset('/')."';
                    </script>";
        }
        
        $this->validate($request, [
            'foto_sk' => 'required|image|mimes:jpeg,jpg,png',
            'jabatan_lama' => 'required',
            'jabatan_baru' => 'required',
            'tanggal_sk' => 'required',
            'nomor_sk' => 'required',
            'penetap' => 'required'
        ]);

        if ($request->hasFile('foto_sk')) {
            $gambar = $request->file('foto_sk');
            
            $ext = $gambar->getClientOriginalExtension();
            
            if ($request->file('foto_sk')->isValid()) {
                $foto_name = "sk_" . $request->nomor_sk . date('YmdHis') . ".$ext";
                $path = 'asset/foto-sk/';
                $request->file('foto_sk')->move($path, $foto_name);
            }
        }

        $promosi = Promosi::firstOrCreate([
            'tanggal_sk' => $request->tanggal_sk,
            'jabatan_baru' => $request->jabatan_baru,
            'foto_sk' => $foto_name,
            'nomor_sk' => $request->nomor_sk,
            'jabatan_lama' => $request->jabatan_lama,
            'penetap' => $request->penetap,
            'pekerja_id' => $request->pekerja_id
        ]);
        
        
        Session::flash("flash_notif", [
            "level"     => "dismissible alert-success",
            "massage"   => "Berhasil, menyimpan data kenaikan pangkat pegawai"
        ]);

        return redirect('promosi/'. $request->pekerja_id);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Promosi  $promosi
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $modul_id = 76;
        $cek = Permission::where('modul_id', $modul_id)
                ->where('group_id', Auth::user()->group_id)->first();

        if ( $cek != "" ) {
            if ( $modul_id != $cek->modul_id) {
                return "<script language='javascript'>
                        alert('Anda Tidak Memiliki Akses');
                        document.location='".asset('/')."';
                        </script>";
            }
        } else {
            return "<script language='javascript'>
                    alert('Anda Tidak Memiliki Akses');
                    document.location='".asset('/')."';
                    </script>";
        }
        
        $title = 'Sistem Informasi Pegawai Dinas Perumahan dan Permukiman Samarinda';
        $route = 'promosi';
        $button = 'Tambah';

        $promosi = Promosi::where('pekerja_id' , $id)->orderby('id','desc')->paginate(50);
        return view('promosi.index', compact('title', 'title', 'route', 'button', 'promosi'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Promosi  $promosi
     * @return \Illuminate\Http\Response
     */
    public function edit(Promosi $promosi)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Promosi  $promosi
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Promosi $promosi)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Promosi  $promosi
     * @return \Illuminate\Http\Response
     */
    public function destroy(Promosi $promosi)
    {
        $modul_id = 77;
        $cek = Permission::where('modul_id', $modul_id)
                ->where('group_id', Auth::user()->group_id)->first();

        if ( $cek != "" ) {
            if ( $modul_id != $cek->modul_id) {
                return "<script language='javascript'>
                        alert('Anda Tidak Memiliki Akses');
                        document.location='".asset('/')."';
                        </script>";
            }
        } else {
            return "<script language='javascript'>
                    alert('Anda Tidak Memiliki Akses');
                    document.location='".asset('/')."';
                    </script>";
        }
        
        $data = $promosi;
        $data->delete();
        Session::flash(
            "flash_notif",[
                "level"   => "dismissible alert-info",
                "massage" => "Berhasil Mengapus Data Kenaikan Pangkat <strong>" . $data->pekerja->nama_pegawai .", Nomor SK $data->nomor_sk</strong>"
        ]);
        
        return redirect('promosi/'. $data->pekerja_id);
    }
}
