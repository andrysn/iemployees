<?php

namespace App\Http\Controllers;

use Auth;
use Session;

use App\Models\Kredit;
use App\Models\Permission;
use Illuminate\Http\Request;

class KreditController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $modul_id = 26;
        $cek = Permission::where('modul_id', $modul_id)
                ->where('group_id', Auth::user()->group_id)->first();

        if ( $cek != "" ) {
            if ( $modul_id != $cek->modul_id) {
                return "<script language='javascript'>
                        alert('Anda Tidak Memiliki Akses');
                        document.location='".asset('/')."';
                        </script>";
            }
        } else {
            return "<script language='javascript'>
                    alert('Anda Tidak Memiliki Akses');
                    document.location='".asset('/')."';
                    </script>";
        }
        
        $this->validate($request, [
            'angka_kredit_lama' => 'required',
            'angka_kredit_baru' => 'required',
            'jumlah' => 'required',
            'penetapan' => 'required'
        ]);

        $kredit = Kredit::firstOrCreate([
            'angka_kredit_lama' => $request->angka_kredit_lama,
            'angka_kredit_baru' => $request->angka_kredit_baru,
            'jumlah' => $request->jumlah,
            'penetapan' => $request->penetapan,
            'pekerja_id' => $request->pekerja_id
        ]);
        
        
        Session::flash("flash_notif", [
            "level"     => "dismissible alert-success",
            "massage"   => "Berhasil, menyimpan data keluarga" . $kredit->pekerja->nama_pegawai
        ]);

        return redirect('kredit/'. $request->pekerja_id);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Kredit  $kredit
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $modul_id = 27;
        $cek = Permission::where('modul_id', $modul_id)
                ->where('group_id', Auth::user()->group_id)->first();

        if ( $cek != "" ) {
            if ( $modul_id != $cek->modul_id) {
                return "<script language='javascript'>
                        alert('Anda Tidak Memiliki Akses');
                        document.location='".asset('/')."';
                        </script>";
            }
        } else {
            return "<script language='javascript'>
                    alert('Anda Tidak Memiliki Akses');
                    document.location='".asset('/')."';
                    </script>";
        }
        
        $title = 'Sistem Informasi Pegawai Dinas Perumahan dan Permukiman Samarinda';
        $route = 'kredit';
        $button = 'Tambah';

        $kredit = Kredit::where('pekerja_id' , $id)->orderby('id','desc')->paginate(50);
        return view('kredit.index', compact('title', 'title', 'route', 'button', 'kredit'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Kredit  $kredit
     * @return \Illuminate\Http\Response
     */
    public function edit(Kredit $kredit)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Kredit  $kredit
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Kredit $kredit)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Kredit  $kredit
     * @return \Illuminate\Http\Response
     */
    public function destroy(Kredit $kredit)
    {
        $modul_id = 28;
        $cek = Permission::where('modul_id', $modul_id)
                ->where('group_id', Auth::user()->group_id)->first();

        if ( $cek != "" ) {
            if ( $modul_id != $cek->modul_id) {
                return "<script language='javascript'>
                        alert('Anda Tidak Memiliki Akses');
                        document.location='".asset('/')."';
                        </script>";
            }
        } else {
            return "<script language='javascript'>
                    alert('Anda Tidak Memiliki Akses');
                    document.location='".asset('/')."';
                    </script>";
        }
        
        $data = $kredit;
        $data->delete();
        Session::flash(
            "flash_notif",[
                "level"   => "dismissible alert-info",
                "massage" => "Berhasil Mengapus Data Angka Kredit <strong>" . $data->pekerja->nama_pegawai ."</strong>"
        ]);
        
        return redirect('kredit/'. $data->pekerja_id);
    }
}
