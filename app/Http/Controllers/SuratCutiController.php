<?php

namespace App\Http\Controllers;

use Auth;
use Session;

use App\Models\Cuti;
use App\Models\Pekerja;
use App\Models\SuratCuti;
use App\Models\Permission;
use Illuminate\Http\Request;

class SuratCutiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $modul_id = 66;
        $cek = Permission::where('modul_id', $modul_id)
                ->where('group_id', Auth::user()->group_id)->first();

        if ( $cek != "" ) {
            if ( $modul_id != $cek->modul_id) {
                return "<script language='javascript'>
                        alert('Anda Tidak Memiliki Akses');
                        document.location='".asset('/')."';
                        </script>";
            }
        } else {
            return "<script language='javascript'>
                    alert('Anda Tidak Memiliki Akses');
                    document.location='".asset('/')."';
                    </script>";
        }
        
        $title = 'Sistem Informasi Pegawai Dinas Perumahan dan Permukiman Samarinda';
        $route = 'surat-cuti';

        $suratCuti = SuratCuti::orderby('id','asc')->paginate(50);
        return view('surat-cuti.index', compact('title', 'title', 'route', 'suratCuti'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $modul_id = 67;
        $cek = Permission::where('modul_id', $modul_id)
                ->where('group_id', Auth::user()->group_id)->first();

        if ( $cek != "" ) {
            if ( $modul_id != $cek->modul_id) {
                return "<script language='javascript'>
                        alert('Anda Tidak Memiliki Akses');
                        document.location='".asset('/')."';
                        </script>";
            }
        } else {
            return "<script language='javascript'>
                    alert('Anda Tidak Memiliki Akses');
                    document.location='".asset('/')."';
                    </script>";
        }

        $title = 'Sistem Informasi Pegawai Dinas Perumahan dan Permukiman Samarinda';
        $route = 'surat-cuti';
        $button = 'Tambah';

        $cuti = Cuti::pluck('keterangan_cuti', 'id')->all();
        $pekerja = Pekerja::pluck('nip', 'id')->all();

        return view('template.create', compact('title', 'button', 'route', 'pekerja', 'cuti'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $modul_id = 67;
        $cek = Permission::where('modul_id', $modul_id)
                ->where('group_id', Auth::user()->group_id)->first();

        if ( $cek != "" ) {
            if ( $modul_id != $cek->modul_id) {
                return "<script language='javascript'>
                        alert('Anda Tidak Memiliki Akses');
                        document.location='".asset('/')."';
                        </script>";
            }
        } else {
            return "<script language='javascript'>
                    alert('Anda Tidak Memiliki Akses');
                    document.location='".asset('/')."';
                    </script>";
        }

        $this->validate($request, [
            'pekerja_id' => 'required',
            'cuti_id' => 'required',
            'tanggal_cuti' => 'required',
            'tanggal_kembali' => 'required'
        ]);
        
        $suratCuti = SuratCuti::firstOrCreate([
            'pekerja_id' => $request->pekerja_id,
            'cuti_id' => $request->cuti_id,
            'tanggal_cuti' => $request->tanggal_cuti,
            'tanggal_kembali' => $request->tanggal_kembali,
        ]);
        
        Session::flash("flash_notif", [
            "level"     => "dismissible alert-success",
            "message"   => "Berhasil, menyimpan data Surat Cuti <strong>" . $suratCuti->pegawai->nama_pegawai . "</strong>" . $suratCuti->cuti->keterangan_cuti
        ]);

        return \Redirect::route('jabatan.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SuratCuti  $suratCuti
     * @return \Illuminate\Http\Response
     */
    public function show(SuratCuti $suratCuti)
    {
        $modul_id = 68;
        $cek = Permission::where('modul_id', $modul_id)
                ->where('group_id', Auth::user()->group_id)->first();

        if ( $cek != "" ) {
            if ( $modul_id != $cek->modul_id) {
                return "<script language='javascript'>
                        alert('Anda Tidak Memiliki Akses');
                        document.location='".asset('/')."';
                        </script>";
            }
        } else {
            return "<script language='javascript'>
                    alert('Anda Tidak Memiliki Akses');
                    document.location='".asset('/')."';
                    </script>";
        }

        $data = $suratCuti;
        $title = 'Sistem Informasi Pegawai Dinas Perumahan dan Permukiman Samarinda';
        $route = 'surat-cuti';
        
        return view('surat-cuti.show', compact('title', 'route', 'data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\SuratCuti  $suratCuti
     * @return \Illuminate\Http\Response
     */
    public function edit(SuratCuti $suratCuti)
    {
        $modul_id = 69;
        $cek = Permission::where('modul_id', $modul_id)
                ->where('group_id', Auth::user()->group_id)->first();

        if ( $cek != "" ) {
            if ( $modul_id != $cek->modul_id) {
                return "<script language='javascript'>
                        alert('Anda Tidak Memiliki Akses');
                        document.location='".asset('/')."';
                        </script>";
            }
        } else {
            return "<script language='javascript'>
                    alert('Anda Tidak Memiliki Akses');
                    document.location='".asset('/')."';
                    </script>";
        }

        $controller = 'SuratCutiController';
        $title = 'Sistem Informasi Pegawai Dinas Perumahan dan Permukiman Samarinda';
        $route = 'surat-cuti';
        $button = 'Perbarui';
        $data = $suratCuti;

        $cuti = Cuti::pluck('keterangan_cuti', 'id')->all();
        $pekerja = Pekerja::pluck('nip', 'id')->all();
        
        return view('template.edit', compact('title', 'button', 'route', 'controller', 'data', 'pekerja', 'cuti'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\SuratCuti  $suratCuti
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SuratCuti $suratCuti)
    {
        $modul_id = 69;
        $cek = Permission::where('modul_id', $modul_id)
                ->where('group_id', Auth::user()->group_id)->first();

        if ( $cek != "" ) {
            if ( $modul_id != $cek->modul_id) {
                return "<script language='javascript'>
                        alert('Anda Tidak Memiliki Akses');
                        document.location='".asset('/')."';
                        </script>";
            }
        } else {
            return "<script language='javascript'>
                    alert('Anda Tidak Memiliki Akses');
                    document.location='".asset('/')."';
                    </script>";
        }

        $this->validate($request, [
            'pekerja_id' => 'required',
            'cuti_id' => 'required',
            'tanggal_cuti' => 'required',
            'tanggal_kembali' => 'required'
        ]);
        
        $data = $suratCuti;
        $data->pekerja_id = $request->pekerja_id;
        $data->cuti_id = $request->cuti_id;
        $data->tanggal_cuti = $request->tanggal_cuti;
        $data->tanggal_kembali = $request->tanggal_kembali;
        $data->update();

        Session::flash(
            "flash_notif", [
                "level"     => "dismissible alert-info",
                "message"   => "Berhasil, memperbarui data surat cuti <strong>" . $data->pekerja->nama_pegawai . "</strong>" . $data->cuti->keterangan_cuti
            ]);
        
        return \Redirect::route('surat-cuti.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SuratCuti  $suratCuti
     * @return \Illuminate\Http\Response
     */
    public function destroy(SuratCuti $suratCuti)
    {
        $modul_id = 70;
        $cek = Permission::where('modul_id', $modul_id)
                ->where('group_id', Auth::user()->group_id)->first();

        if ( $cek != "" ) {
            if ( $modul_id != $cek->modul_id) {
                return "<script language='javascript'>
                        alert('Anda Tidak Memiliki Akses');
                        document.location='".asset('/')."';
                        </script>";
            }
        } else {
            return "<script language='javascript'>
                    alert('Anda Tidak Memiliki Akses');
                    document.location='".asset('/')."';
                    </script>";
        }
        
        $data = $suratCuti;
        $data->delete();
        Session::flash(
            "flash_notif",[
                "level"   => "dismissible alert-info",
                "massage" => "Berhasil Menghapus surat cuti <strong>" . $data->pekerja->nama_pegawai . "</strong>" . $data->cuti->keterangan_cuti
        ]);
        
        return \Redirect::route('surat-cuti.index');
    }
}