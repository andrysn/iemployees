<?php

namespace App\Http\Controllers;

use Auth;
use Session;

use App\Models\Bidang;
use App\Models\SubBidang;
use App\Models\Permission;
use Illuminate\Http\Request;

class SubBidangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $modul_id = 61;
        $cek = Permission::where('modul_id', $modul_id)
                ->where('group_id', Auth::user()->group_id)->first();

        if ( $cek != "" ) {
            if ( $modul_id != $cek->modul_id) {
                return "<script language='javascript'>
                        alert('Anda Tidak Memiliki Akses');
                        document.location='".asset('/')."';
                        </script>";
            }
        } else {
            return "<script language='javascript'>
                    alert('Anda Tidak Memiliki Akses');
                    document.location='".asset('/')."';
                    </script>";
        }
        
        $title = 'Sistem Informasi Pegawai Dinas Perumahan dan Permukiman Samarinda';
        $route = 'sub-bidang';

        $subBidang = SubBidang::orderby('id','desc')->paginate(50);
        return view('sub-bidang.index', compact('title', 'title', 'route', 'subBidang'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $modul_id = 62;
        $cek = Permission::where('modul_id', $modul_id)
                ->where('group_id', Auth::user()->group_id)->first();

        if ( $cek != "" ) {
            if ( $modul_id != $cek->modul_id) {
                return "<script language='javascript'>
                        alert('Anda Tidak Memiliki Akses');
                        document.location='".asset('/')."';
                        </script>";
            }
        } else {
            return "<script language='javascript'>
                    alert('Anda Tidak Memiliki Akses');
                    document.location='".asset('/')."';
                    </script>";
        }

        $title = 'Sistem Informasi Pegawai Dinas Perumahan dan Permukiman Samarinda';
        $route = 'sub-bidang';
        $button = 'Tambah';
        $bidang = Bidang::pluck('nama_bidang', 'id')->all();

        return view('template.create', compact('title', 'button', 'route', 'bidang'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $modul_id = 62;
        $cek = Permission::where('modul_id', $modul_id)
                ->where('group_id', Auth::user()->group_id)->first();

        if ( $cek != "" ) {
            if ( $modul_id != $cek->modul_id) {
                return "<script language='javascript'>
                        alert('Anda Tidak Memiliki Akses');
                        document.location='".asset('/')."';
                        </script>";
            }
        } else {
            return "<script language='javascript'>
                    alert('Anda Tidak Memiliki Akses');
                    document.location='".asset('/')."';
                    </script>";
        }

        $this->validate($request, [
            'nama_sub_bidang' => 'required|unique:sub_bidangs',
            'kode_sub_bidang' => 'required',
            'bidang_id' => 'required'
        ]);
        
        $subBidang = SubBidang::firstOrCreate([
            'bidang_id' => $request->bidang_id,
            'kode_sub_bidang' => $request->kode_sub_bidang,
            'nama_sub_bidang' => $request->nama_sub_bidang
        ]);
        
        
        Session::flash("flash_notif", [
            "level"     => "dismissible alert-success",
            "message"   => "Berhasil, menyimpan data sub bidang <strong>$subBidang->kode_sub_bidang, $subBidang->nama_sub_bidang</strong>"
        ]);

        return \Redirect::route('sub-bidang.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SubBidang  $subBidang
     * @return \Illuminate\Http\Response
     */
    public function show(SubBidang $subBidang)
    {
        $modul_id = 63;
        // $cek = Permission::where('modul_id', $modul_id)
        //         ->where('group_id', Auth::user()->group_id)->first();

        // if ( $cek != "" ) {
        //     if ( $modul_id != $cek->modul_id) {
        //         return "<script language='javascript'>
        //                 alert('Anda Tidak Memiliki Akses');
        //                 document.location='".asset('/')."';
        //                 </script>";
        //     }
        // } else {
        //     return "<script language='javascript'>
        //             alert('Anda Tidak Memiliki Akses');
        //             document.location='".asset('/')."';
        //             </script>";
        // }

        $data = $subBidang;
        $title = 'Sistem Informasi Pegawai Dinas Perumahan dan Permukiman Samarinda';
        $route = 'sub-bidang';
        
        return view('sub-bidang.show', compact('title', 'route', 'data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\SubBidang  $subBidang
     * @return \Illuminate\Http\Response
     */
    public function edit(SubBidang $subBidang)
    {
        $modul_id = 64;
        $cek = Permission::where('modul_id', $modul_id)
                ->where('group_id', Auth::user()->group_id)->first();

        if ( $cek != "" ) {
            if ( $modul_id != $cek->modul_id) {
                return "<script language='javascript'>
                        alert('Anda Tidak Memiliki Akses');
                        document.location='".asset('/')."';
                        </script>";
            }
        } else {
            return "<script language='javascript'>
                    alert('Anda Tidak Memiliki Akses');
                    document.location='".asset('/')."';
                    </script>";
        }

        $controller = 'SubBidangController';
        $title = 'Sistem Informasi Pegawai Dinas Perumahan dan Permukiman Samarinda';
        $route = 'sub-bidang';
        $button = 'Perbarui';
        $data = $subBidang;
        $bidang = Bidang::pluck('nama_bidang', 'id')->all();
        
        return view('template.edit', compact('title', 'button', 'route', 'controller', 'data', 'bidang'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\SubBidang  $subBidang
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SubBidang $subBidang)
    {
        $modul_id = 64;
        $cek = Permission::where('modul_id', $modul_id)
                ->where('group_id', Auth::user()->group_id)->first();

        if ( $cek != "" ) {
            if ( $modul_id != $cek->modul_id) {
                return "<script language='javascript'>
                        alert('Anda Tidak Memiliki Akses');
                        document.location='".asset('/')."';
                        </script>";
            }
        } else {
            return "<script language='javascript'>
                    alert('Anda Tidak Memiliki Akses');
                    document.location='".asset('/')."';
                    </script>";
        }

        $this->validate($request, [
            'nama_sub_bidang' => 'required|unique:bidangs,nama_bidang,'.$subBidang->id,
            'kode_sub_bidang' => 'required',
            'bidang_id' => 'required'
        ]);
        
        $data = $subBidang;
        $data->nama_sub_bidang = $request->nama_sub_bidang;
        $data->kode_sub_bidang = $request->kode_sub_bidang;
        $data->bidang_id = $request->bidang_id;
        $data->update();

        Session::flash(
            "flash_notif", [
                "level"     => "dismissible alert-info",
                "message"   => "Berhasil, memperbarui data bidang <strong>$request->kode_sub_bidang, $request->nama_sub_bidang</strong>"
            ]);
        
        return \Redirect::route('sub-bidang.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SubBidang  $subBidang
     * @return \Illuminate\Http\Response
     */
    public function destroy(SubBidang $subBidang)
    {
        $modul_id = 65;
        $cek = Permission::where('modul_id', $modul_id)
                ->where('group_id', Auth::user()->group_id)->first();

        if ( $cek != "" ) {
            if ( $modul_id != $cek->modul_id) {
                return "<script language='javascript'>
                        alert('Anda Tidak Memiliki Akses');
                        document.location='".asset('/')."';
                        </script>";
            }
        } else {
            return "<script language='javascript'>
                    alert('Anda Tidak Memiliki Akses');
                    document.location='".asset('/')."';
                    </script>";
        }
        
        $data = $subBidang;
        $data->delete();
        Session::flash(
            "flash_notif",[
                "level"   => "dismissible alert-info",
                "massage" => "Berhasil Mengapus Data Sub Bidang <strong>$data->nama_sub_bidang, $data->kode_sub_bidang</strong>"
        ]);
        
        return \Redirect::route('sub-bidang.index');
    }
}
