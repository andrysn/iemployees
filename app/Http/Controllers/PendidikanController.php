<?php

namespace App\Http\Controllers;

use Auth;
use Session;

use App\Models\Pendidikan;
use App\Models\Permission;
use Illuminate\Http\Request;

class PendidikanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $modul_id = 47;
        $cek = Permission::where('modul_id', $modul_id)
                ->where('group_id', Auth::user()->group_id)->first();

        if ( $cek != "" ) {
            if ( $modul_id != $cek->modul_id) {
                return "<script language='javascript'>
                        alert('Anda Tidak Memiliki Akses');
                        document.location='".asset('/')."';
                        </script>";
            }
        } else {
            return "<script language='javascript'>
                    alert('Anda Tidak Memiliki Akses');
                    document.location='".asset('/')."';
                    </script>";
        }
        
        $this->validate($request, [
            'foto_ijazah' => 'required|image|mimes:jpeg,jpg,png',
            'jenjang' => 'required',
            'nomor_ijazah' => 'required',
            'nama_akademi' => 'required'
        ]);

        if ($request->hasFile('foto_ijazah')) {
            $gambar = $request->file('foto_ijazah');
            
            $ext = $gambar->getClientOriginalExtension();
            
            if ($request->file('foto_ijazah')->isValid()) {
                $foto_name = "ijazah_" . $request->nomor_ijazah . date('YmdHis') . ".$ext";
                $path = 'asset/foto-ijazah/';
                $request->file('foto_ijazah')->move($path, $foto_name);
            }
        }

        $pendidikan = Pendidikan::firstOrCreate([
            'jenjang' => $request->jenjang,
            'nomor_ijazah' => $request->nomor_ijazah,
            'foto_ijazah' => $foto_name,
            'nama_akademi' => $request->nama_akademi,
            'pekerja_id' => $request->pekerja_id
        ]);
        
        
        Session::flash("flash_notif", [
            "level"     => "dismissible alert-success",
            "massage"   => "Berhasil, menyimpan data pendidikan pegawai"
        ]);

        return redirect('pendidikan/'. $request->pekerja_id);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Pendidikan  $pendidikan
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $modul_id = 48;
        $cek = Permission::where('modul_id', $modul_id)
                ->where('group_id', Auth::user()->group_id)->first();

        if ( $cek != "" ) {
            if ( $modul_id != $cek->modul_id) {
                return "<script language='javascript'>
                        alert('Anda Tidak Memiliki Akses');
                        document.location='".asset('/')."';
                        </script>";
            }
        } else {
            return "<script language='javascript'>
                    alert('Anda Tidak Memiliki Akses');
                    document.location='".asset('/')."';
                    </script>";
        }
        
        $title = 'Sistem Informasi Pegawai Dinas Perumahan dan Permukiman Samarinda';
        $route = 'pendidikan';
        $button = 'Tambah';

        $pendidikan = Pendidikan::where('pekerja_id' , $id)->orderby('id','desc')->paginate(50);
        return view('pendidikan.index', compact('title', 'title', 'route', 'button', 'pendidikan'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Pendidikan  $pendidikan
     * @return \Illuminate\Http\Response
     */
    public function edit(Pendidikan $pendidikan)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Pendidikan  $pendidikan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Pendidikan $pendidikan)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Pendidikan  $pendidikan
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pendidikan $pendidikan)
    {
        $modul_id = 49;
        $cek = Permission::where('modul_id', $modul_id)
                ->where('group_id', Auth::user()->group_id)->first();

        if ( $cek != "" ) {
            if ( $modul_id != $cek->modul_id) {
                return "<script language='javascript'>
                        alert('Anda Tidak Memiliki Akses');
                        document.location='".asset('/')."';
                        </script>";
            }
        } else {
            return "<script language='javascript'>
                    alert('Anda Tidak Memiliki Akses');
                    document.location='".asset('/')."';
                    </script>";
        }
        
        $data = $pendidikan;
        $data->delete();
        Session::flash(
            "flash_notif",[
                "level"   => "dismissible alert-info",
                "massage" => "Berhasil Mengapus Data Pendidikan <strong>" . $data->pekerja->nama_pekerja .", Jenjang $data->jenjang</strong>"
        ]);
        
        return redirect('pendidikan/'. $data->pekerja_id);
    }
}
