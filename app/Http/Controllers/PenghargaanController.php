<?php

namespace App\Http\Controllers;

use Auth;
use Session;

use App\Models\Permission;
use App\Models\Penghargaan;
use Illuminate\Http\Request;

class PenghargaanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $modul_id = 50;
        $cek = Permission::where('modul_id', $modul_id)
                ->where('group_id', Auth::user()->group_id)->first();

        if ( $cek != "" ) {
            if ( $modul_id != $cek->modul_id) {
                return "<script language='javascript'>
                        alert('Anda Tidak Memiliki Akses');
                        document.location='".asset('/')."';
                        </script>";
            }
        } else {
            return "<script language='javascript'>
                    alert('Anda Tidak Memiliki Akses');
                    document.location='".asset('/')."';
                    </script>";
        }
        
        $this->validate($request, [
            'nama_penghargaan' => 'required',
            'tanggal_penghargaan' => 'required',
            'instansi_pemberi' => 'required',
            'foto_trophy' => 'required'
        ]);

        if ($request->hasFile('foto_trophy')) {
            $gambar = $request->file('foto_trophy');
            
            $ext = $gambar->getClientOriginalExtension();
            
            if ($request->file('foto_trophy')->isValid()) {
                $foto_name = "trophy_" . $request->pekerja_id . date('YmdHis') . ".$ext";
                $path = 'asset/foto-trophy/';
                $request->file('foto_trophy')->move($path, $foto_name);
            }
        }

        $penghargaan = Penghargaan::firstOrCreate([
            'tanggal_penghargaan' => $request->tanggal_penghargaan,
            'nama_penghargaan' => $request->nama_penghargaan,
            'instansi_pemberi' => $request->instansi_pemberi,
            'foto_trophy' => $foto_name,
            'pekerja_id' => $request->pekerja_id
        ]);
        
        
        Session::flash("flash_notif", [
            "level"     => "dismissible alert-success",
            "massage"   => "Berhasil, menyimpan data penghargaan" . $penghargaan->pekerja->nama_pegawai
        ]);

        return redirect('penghargaan/'. $request->pekerja_id);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Penghargaan  $penghargaan
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $modul_id = 51;
        $cek = Permission::where('modul_id', $modul_id)
                ->where('group_id', Auth::user()->group_id)->first();

        if ( $cek != "" ) {
            if ( $modul_id != $cek->modul_id) {
                return "<script language='javascript'>
                        alert('Anda Tidak Memiliki Akses');
                        document.location='".asset('/')."';
                        </script>";
            }
        } else {
            return "<script language='javascript'>
                    alert('Anda Tidak Memiliki Akses');
                    document.location='".asset('/')."';
                    </script>";
        }
        
        $title = 'Sistem Informasi Pegawai Dinas Perumahan dan Permukiman Samarinda';
        $route = 'penghargaan';
        $button = 'Tambah';

        $penghargaan = Penghargaan::where('pekerja_id' , $id)->orderby('id','desc')->paginate(50);
        return view('penghargaan.index', compact('title', 'title', 'route', 'button', 'penghargaan'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Penghargaan  $penghargaan
     * @return \Illuminate\Http\Response
     */
    public function edit(Penghargaan $penghargaan)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Penghargaan  $penghargaan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Penghargaan $penghargaan)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Penghargaan  $penghargaan
     * @return \Illuminate\Http\Response
     */
    public function destroy(Penghargaan $penghargaan)
    {
        $modul_id = 52;
        $cek = Permission::where('modul_id', $modul_id)
                ->where('group_id', Auth::user()->group_id)->first();

        if ( $cek != "" ) {
            if ( $modul_id != $cek->modul_id) {
                return "<script language='javascript'>
                        alert('Anda Tidak Memiliki Akses');
                        document.location='".asset('/')."';
                        </script>";
            }
        } else {
            return "<script language='javascript'>
                    alert('Anda Tidak Memiliki Akses');
                    document.location='".asset('/')."';
                    </script>";
        }
        
        $data = $penghargaan;
        $data->delete();
        Session::flash(
            "flash_notif",[
                "level"   => "dismissible alert-info",
                "massage" => "Berhasil Menghapus Penghargaan <strong>" . $data->pekerja->nama_pegawai ."</strong>"
        ]);
        
        return redirect('penghargaan/'. $data->pekerja_id);
    }
}
