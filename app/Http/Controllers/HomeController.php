<?php

namespace App\Http\Controllers;

use Auth;
use Session;

use App\Models\Cuti;
use App\Models\Pekerja;
use App\Models\Permission;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = 'Sistem Informasi Pegawai Dinas Perumahan dan Permukiman Samarinda';

        $aktifPegawai = Pekerja::where('status', '=','1')->count();
        $nonAktifPegawai = Pekerja::where('status', '=','2')->count();
        $cuti = Cuti::all()->count();

        return view('dashboard', compact('title', 'aktifPegawai', 'nonAktifPegawai', 'cuti'));
    }
}
