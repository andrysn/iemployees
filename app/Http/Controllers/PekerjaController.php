<?php

namespace App\Http\Controllers;

use Auth;
use Session;

use App\Models\Bidang;
use App\Models\Pekerja;
use App\Models\Jabatan;
use App\Models\Permission;
use App\Models\StatusPegawai;
use App\Models\PangkatEselon;
use App\Models\PangkatGolongan;
use Illuminate\Http\Request;

class PekerjaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $modul_id = 42;
        $cek = Permission::where('modul_id', $modul_id)
                ->where('group_id', Auth::user()->group_id)->first();

        if ( $cek != "" ) {
            if ( $modul_id != $cek->modul_id) {
                return "<script language='javascript'>
                        alert('Anda Tidak Memiliki Akses');
                        document.location='".asset('/')."';
                        </script>";
            }
        } else {
            return "<script language='javascript'>
                    alert('Anda Tidak Memiliki Akses');
                    document.location='".asset('/')."';
                    </script>";
        }
        
        $title = 'Sistem Informasi Pegawai Dinas Perumahan dan Permukiman Samarinda';
        $route = 'pekerja';

        $pekerja = Pekerja::orderby('id','asc')->paginate(50);
        return view('pekerja.index', compact('title', 'title', 'route', 'pekerja'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $modul_id = 43;
        $cek = Permission::where('modul_id', $modul_id)
                ->where('group_id', Auth::user()->group_id)->first();

        if ( $cek != "" ) {
            if ( $modul_id != $cek->modul_id) {
                return "<script language='javascript'>
                        alert('Anda Tidak Memiliki Akses');
                        document.location='".asset('/')."';
                        </script>";
            }
        } else {
            return "<script language='javascript'>
                    alert('Anda Tidak Memiliki Akses');
                    document.location='".asset('/')."';
                    </script>";
        }

        $title = 'Sistem Informasi Pegawai Dinas Perumahan dan Permukiman Samarinda';
        $route = 'pekerja';
        $button = 'Tambah';

        $bidang = Bidang::pluck('nama_bidang', 'id')->all();
        $jabatan = Jabatan::pluck('nama_jabatan', 'id')->all();
        $pangkatEselon = PangkatEselon::pluck('nama_eselon', 'id')->all();
        $pangkatGolongan = PangkatGolongan::pluck('nama_golongan', 'id')->all();
        $statusPegawai = StatusPegawai::pluck('status_pegawai', 'id')->all();

        return view('template.create', compact('title', 'button', 'route', 'bidang', 'jabatan', 'pangkatEselon', 'pangkatGolongan', 'statusPegawai'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $modul_id = 43;
        $cek = Permission::where('modul_id', $modul_id)
                ->where('group_id', Auth::user()->group_id)->first();

        if ( $cek != "" ) {
            if ( $modul_id != $cek->modul_id) {
                return "<script language='javascript'>
                        alert('Anda Tidak Memiliki Akses');
                        document.location='".asset('/')."';
                        </script>";
            }
        } else {
            return "<script language='javascript'>
                    alert('Anda Tidak Memiliki Akses');
                    document.location='".asset('/')."';
                    </script>";
        }

        $this->validate($request, [       
            'nip' => 'required',
            'foto' => 'required|image|mimes:jpeg,jpg,png',
            'jabatan_id' => 'required',
            'bidang_id' => 'required',
            'status_pegawai_id' => 'required',
            'keterangan_pegawai' => 'required',
            'nama_pegawai' => 'required',
            'alamat' => 'required',
            'tempat_lahir' => 'required',
            'tanggal_lahir' => 'required',
            'jenis_kelamin' => 'required',
            'agama' => 'required',
            'nomor_karpeg' => 'required',
            'nomor_bpjs' => 'required',
            'nomor_pokok_wajib_pajak' => 'required'
        ]);

        if ($request->hasFile('foto')) {
            $gambar = $request->file('foto');
            
            $ext = $gambar->getClientOriginalExtension();
            
            if ($request->file('foto')->isValid()) {
                $foto_name = "pegawai_" . $request->nip . ".$ext";
                $path = 'asset/foto-pegawai/';
                $request->file('foto')->move($path, $foto_name);
            }
        }

        $pekerja = Pekerja::firstOrCreate([
            'nip' => $request->nip,
            'bidang_id' => $request->bidang_id,
            'status_pegawai_id' => $request->status_pegawai_id,
            'jabatan_id' => $request->jabatan_id,
            'pangkat_eselon_id' => $request->pangkat_eselon_id,
            'pangkat_golongan_id' => $request->pangkat_golongan_id,
            'foto' => $foto_name,
            'keterangan_pegawai' => $request->keterangan_pegawai,
            'nama_pegawai' => $request->nama_pegawai,
            'alamat' => $request->alamat,
            'tempat_lahir' => $request->tempat_lahir,
            'tanggal_lahir' => $request->tanggal_lahir,
            'jenis_kelamin' => $request->jenis_kelamin,
            'agama' => $request->agama,
            'nomor_karpeg'    => $request->nomor_karpeg,
            'nomor_bpjs'   => $request->nomor_bpjs,
            'nomor_pokok_wajib_pajak'    => $request->nomor_pokok_wajib_pajak
            
        ]);
        
        
        Session::flash("flash_notif", [
            "level"     => "dismissible alert-success",
            "massage"   => "Berhasil, menyimpan data pegawai <strong>$pekerja->nama_pegawai</strong>"
        ]);

        return \Redirect::route('pekerja.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Pekerja  $pekerja
     * @return \Illuminate\Http\Response
     */
    public function show(Pekerja $pekerja)
    {
        $modul_id = 44;
        $cek = Permission::where('modul_id', $modul_id)
                ->where('group_id', Auth::user()->group_id)->first();

        if ( $cek != "" ) {
            if ( $modul_id != $cek->modul_id) {
                return "<script language='javascript'>
                        alert('Anda Tidak Memiliki Akses');
                        document.location='".asset('/')."';
                        </script>";
            }
        } else {
            return "<script language='javascript'>
                    alert('Anda Tidak Memiliki Akses');
                    document.location='".asset('/')."';
                    </script>";
        }

        $data = $pekerja;
        $title = 'Sistem Informasi Pegawai Dinas Perumahan dan Permukiman Samarinda';
        $route = 'pekerja';
        
        return view('pekerja.show', compact('title', 'route', 'data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Pekerja  $pekerja
     * @return \Illuminate\Http\Response
     */
    public function edit(Pekerja $pekerja)
    {
        $modul_id = 45;
        $cek = Permission::where('modul_id', $modul_id)
                ->where('group_id', Auth::user()->group_id)->first();

        if ( $cek != "" ) {
            if ( $modul_id != $cek->modul_id) {
                return "<script language='javascript'>
                        alert('Anda Tidak Memiliki Akses');
                        document.location='".asset('/')."';
                        </script>";
            }
        } else {
            return "<script language='javascript'>
                    alert('Anda Tidak Memiliki Akses');
                    document.location='".asset('/')."';
                    </script>";
        }

        $controller = 'PekerjaController';
        $title = 'Sistem Informasi Pegawai Dinas Perumahan dan Permukiman Samarinda';
        $route = 'pekerja';
        $button = 'Perbarui';
        $data = $pekerja;
        
        $bidang = Bidang::pluck('nama_bidang', 'id')->all();
        $jabatan = Jabatan::pluck('nama_jabatan', 'id')->all();
        $pangkatEselon = PangkatEselon::pluck('nama_eselon', 'id')->all();
        $pangkatGolongan = PangkatGolongan::pluck('nama_golongan', 'id')->all();
        $statusPegawai = StatusPegawai::pluck('status_pegawai', 'id')->all();

        return view('template.edit', compact('title', 'button', 'controller', 'route', 'bidang', 'jabatan', 'pangkatEselon', 'pangkatGolongan', 'statusPegawai', 'data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Pekerja  $pekerja
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Pekerja $pekerja)
    {
        $modul_id = 45;
        $cek = Permission::where('modul_id', $modul_id)
                ->where('group_id', Auth::user()->group_id)->first();

        if ( $cek != "" ) {
            if ( $modul_id != $cek->modul_id) {
                return "<script language='javascript'>
                        alert('Anda Tidak Memiliki Akses');
                        document.location='".asset('/')."';
                        </script>";
            }
        } else {
            return "<script language='javascript'>
                    alert('Anda Tidak Memiliki Akses');
                    document.location='".asset('/')."';
                    </script>";
        }

        $this->validate($request, [       
            'nip' => 'required',
            'jabatan_id' => 'required',
            'bidang_id' => 'required',
            'status_pegawai_id' => 'required',
            'keterangan_pegawai' => 'required',
            'nama_pegawai' => 'required',
            'alamat' => 'required',
            'tempat_lahir' => 'required',
            'tanggal_lahir' => 'required',
            'jenis_kelamin' => 'required',
            'agama' => 'required',
            'nomor_karpeg' => 'required',
            'nomor_bpjs' => 'required',
            'nomor_pokok_wajib_pajak' => 'required'
        ]);
        
        $data = $pekerja;
        $data->jabatan_id = $request->jabatan_id;
        $data->nip = $request->nip;
        $data->bidang_id = $request->bidang_id;
        $data->status_pegawai_id = $request->status_pegawai_id;
        $data->jabatan_id = $request->jabatan_id;
        $data->pangkat_eselon_id = $request->pangkat_eselon_id;
        $data->pangkat_golongan_id = $request->pangkat_golongan_id;
        $data->keterangan_pegawai = $request->keterangan_pegawai;
        $data->nama_pegawai = $request->nama_pegawai;
        $data->alamat = $request->alamat;
        $data->tempat_lahir = $request->tempat_lahir;
        $data->tanggal_lahir = $request->tanggal_lahir;
        $data->jenis_kelamin = $request->jenis_kelamin;
        $data->agama = $request->agama;
        $data->nomor_karpeg = $request->nomor_karpeg;
        $data->nomor_bpjs = $request->nomor_bpjs;
        $data->nomor_pokok_wajib_pajak = $request->nomor_pokok_wajib_pajak;

        if ($request->hasFile('foto')) {
            $gambar = $request->file('foto');
            
            $ext = $gambar->getClientOriginalExtension();
            
            if ($request->file('foto')->isValid()) {
                $foto_name = "pegawai_" . $request->nip . ".$ext";
                $path = 'asset/foto-pegawai/';
                $request->file('foto')->move($path, $foto_name);
            }
            $data->foto = $foto_name;
        }

        $data->update();

        Session::flash(
            "flash_notif", [
                "level"     => "dismissible alert-info",
                "message"   => "Berhasil, memperbarui data pegawai <strong>$request->nama_pegawai</strong>"
            ]);
        
        return \Redirect::route('pekerja.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Pekerja  $pekerja
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pekerja $pekerja)
    {
        $modul_id = 46;
        $cek = Permission::where('modul_id', $modul_id)
                ->where('group_id', Auth::user()->group_id)->first();

        if ( $cek != "" ) {
            if ( $modul_id != $cek->modul_id) {
                return "<script language='javascript'>
                        alert('Anda Tidak Memiliki Akses');
                        document.location='".asset('/')."';
                        </script>";
            }
        } else {
            return "<script language='javascript'>
                    alert('Anda Tidak Memiliki Akses');
                    document.location='".asset('/')."';
                    </script>";
        }

        $data = $pekerja;
        $data->delete();
        Session::flash(
            "flash_notif",[
                "level"   => "dismissible alert-info",
                "massage" => "Berhasil Menghapus Data Pegawai <strong>$data->nama_pegawai</strong>"
        ]);
        
        return \Redirect::route('pekerja.index');
    }
}
