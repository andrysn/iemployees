hh<?php

namespace App\Http\Controllers;

use Auth;
use Session;

use App\Models\Permission;
use App\Models\PangkatEselon;
use Illuminate\Http\Request;

class PangkatEselonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $modul_id = 32;
        $cek = Permission::where('modul_id', $modul_id)
                ->where('group_id', Auth::user()->group_id)->first();

        if ( $cek != "" ) {
            if ( $modul_id != $cek->modul_id) {
                return "<script language='javascript'>
                        alert('Anda Tidak Memiliki Akses');
                        document.location='".asset('/')."';
                        </script>";
            }
        } else {
            return "<script language='javascript'>
                    alert('Anda Tidak Memiliki Akses');
                    document.location='".asset('/')."';
                    </script>";
        }
        
        $title = 'Sistem Informasi Pegawai Dinas Perumahan dan Permukiman Samarinda';
        $route = 'pangkat-eselon';

        $pangkatEselon = PangkatEselon::orderby('id','asc')->paginate(50);
        return view('pangkat-eselon.index', compact('title', 'title', 'route', 'pangkatEselon'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $modul_id = 33;
        $cek = Permission::where('modul_id', $modul_id)
                ->where('group_id', Auth::user()->group_id)->first();

        if ( $cek != "" ) {
            if ( $modul_id != $cek->modul_id) {
                return "<script language='javascript'>
                        alert('Anda Tidak Memiliki Akses');
                        document.location='".asset('/')."';
                        </script>";
            }
        } else {
            return "<script language='javascript'>
                    alert('Anda Tidak Memiliki Akses');
                    document.location='".asset('/')."';
                    </script>";
        }

        $title = 'Sistem Informasi Pegawai Dinas Perumahan dan Permukiman Samarinda';
        $route = 'pangkat-eselon';
        $button = 'Tambah';

        return view('template.create', compact('title', 'button', 'route'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $modul_id = 33;
        $cek = Permission::where('modul_id', $modul_id)
                ->where('group_id', Auth::user()->group_id)->first();

        if ( $cek != "" ) {
            if ( $modul_id != $cek->modul_id) {
                return "<script language='javascript'>
                        alert('Anda Tidak Memiliki Akses');
                        document.location='".asset('/')."';
                        </script>";
            }
        } else {
            return "<script language='javascript'>
                    alert('Anda Tidak Memiliki Akses');
                    document.location='".asset('/')."';
                    </script>";
        }

        $this->validate($request, [
            'nama_eselon' => 'required|unique:pangkat_eselons',
            'keterangan_eselon' => 'required|unique:pangkat_eselons'
        ]);
        
        $pangkatEselon = PangkatEselon::firstOrCreate([
            'nama_eselon' => $request->nama_eselon,
            'keterangan_eselon' => $request->keterangan_eselon
        ]);
        
        
        Session::flash("flash_notif", [
            "level"     => "dismissible alert-success",
            "message"   => "Berhasil, menyimpan data Pangkat Eselon <strong>$pangkatEselon->nama_eselon, $pangkatEselon->keterangan_eselon</strong>"
        ]);

        return \Redirect::route('pangkat-eselon.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PangkatEselon  $pangkatEselon
     * @return \Illuminate\Http\Response
     */
    public function show(PangkatEselon $pangkatEselon)
    {
        $modul_id = 34;
        $cek = Permission::where('modul_id', $modul_id)
                ->where('group_id', Auth::user()->group_id)->first();

        if ( $cek != "" ) {
            if ( $modul_id != $cek->modul_id) {
                return "<script language='javascript'>
                        alert('Anda Tidak Memiliki Akses');
                        document.location='".asset('/')."';
                        </script>";
            }
        } else {
            return "<script language='javascript'>
                    alert('Anda Tidak Memiliki Akses');
                    document.location='".asset('/')."';
                    </script>";
        }

        $data = $pangkatEselon;
        $title = 'Sistem Informasi Pegawai Dinas Perumahan dan Permukiman Samarinda';
        $route = 'pangkat-eselon';
        
        return view('pangkat-eselon.show', compact('title', 'route', 'data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\PangkatEselon  $pangkatEselon
     * @return \Illuminate\Http\Response
     */
    public function edit(PangkatEselon $pangkatEselon)
    {
        $modul_id = 35;
        $cek = Permission::where('modul_id', $modul_id)
                ->where('group_id', Auth::user()->group_id)->first();

        if ( $cek != "" ) {
            if ( $modul_id != $cek->modul_id) {
                return "<script language='javascript'>
                        alert('Anda Tidak Memiliki Akses');
                        document.location='".asset('/')."';
                        </script>";
            }
        } else {
            return "<script language='javascript'>
                    alert('Anda Tidak Memiliki Akses');
                    document.location='".asset('/')."';
                    </script>";
        }

        $controller = 'PangkatEselonController';
        $title = 'Sistem Informasi Pegawai Dinas Perumahan dan Permukiman Samarinda';
        $route = 'pangkat-eselon';
        $button = 'Perbarui';
        $data = $pangkatEselon;
        
        return view('template.edit', compact('title', 'button', 'route', 'controller', 'data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\PangkatEselon  $pangkatEselon
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PangkatEselon $pangkatEselon)
    {
        $modul_id = 35;
        $cek = Permission::where('modul_id', $modul_id)
                ->where('group_id', Auth::user()->group_id)->first();

        if ( $cek != "" ) {
            if ( $modul_id != $cek->modul_id) {
                return "<script language='javascript'>
                        alert('Anda Tidak Memiliki Akses');
                        document.location='".asset('/')."';
                        </script>";
            }
        } else {
            return "<script language='javascript'>
                    alert('Anda Tidak Memiliki Akses');
                    document.location='".asset('/')."';
                    </script>";
        }

        $this->validate($request, [
            'nama_eselon' => 'required|unique:pangkat_eselons,nama_eselon,'.$pangkatEselon->id,
            'keterangan_eselon' => 'required|unique:pangkat_eselons,keterangan_eselon,'.$pangkatEselon->id
        ]);
        
        $data = $pangkatEselon;
        $data->nama_eselon = $request->nama_eselon;
        $data->keterangan_eselon = $request->keterangan_eselon;
        $data->update();

        Session::flash(
            "flash_notif", [
                "level"     => "dismissible alert-info",
                "message"   => "Berhasil, memperbarui data pangkat eselon <strong>$request->nama_eselon, $request->keterangan_eselon</strong>"
            ]);
        
        return \Redirect::route('pangkat-eselon.index');    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PangkatEselon  $pangkatEselon
     * @return \Illuminate\Http\Response
     */
    public function destroy(PangkatEselon $pangkatEselon)
    {
        $modul_id = 36;
        $cek = Permission::where('modul_id', $modul_id)
                ->where('group_id', Auth::user()->group_id)->first();

        if ( $cek != "" ) {
            if ( $modul_id != $cek->modul_id) {
                return "<script language='javascript'>
                        alert('Anda Tidak Memiliki Akses');
                        document.location='".asset('/')."';
                        </script>";
            }
        } else {
            return "<script language='javascript'>
                    alert('Anda Tidak Memiliki Akses');
                    document.location='".asset('/')."';
                    </script>";
        }
        
        $data = $pangkatEselon;
        $data->delete();
        Session::flash(
            "flash_notif",[
                "level"   => "dismissible alert-info",
                "massage" => "Berhasil Mengapus Data Pangkat Eselon <strong>$data->nama_eselon, $data->keterangan_eselon</strong>"
        ]);
        
        return \Redirect::route('pangkat-eselon.index');
    }
}
