<?php

namespace App\Http\Controllers;

use Auth;
use Session;

use App\Models\Permission;
use App\Models\PangkatGolongan;
use Illuminate\Http\Request;

class PangkatGolonganController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $modul_id = 37;
        $cek = Permission::where('modul_id', $modul_id)
                ->where('group_id', Auth::user()->group_id)->first();

        if ( $cek != "" ) {
            if ( $modul_id != $cek->modul_id) {
                return "<script language='javascript'>
                        alert('Anda Tidak Memiliki Akses');
                        document.location='".asset('/')."';
                        </script>";
            }
        } else {
            return "<script language='javascript'>
                    alert('Anda Tidak Memiliki Akses');
                    document.location='".asset('/')."';
                    </script>";
        }
        
        $title = 'Sistem Informasi Pegawai Dinas Perumahan dan Permukiman Samarinda';
        $route = 'pangkat-golongan';

        $pangkatGolongan = PangkatGolongan::orderby('id','asc')->paginate(50);
        return view('pangkat-golongan.index', compact('title', 'title', 'route', 'pangkatGolongan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $modul_id = 38;
        $cek = Permission::where('modul_id', $modul_id)
                ->where('group_id', Auth::user()->group_id)->first();

        if ( $cek != "" ) {
            if ( $modul_id != $cek->modul_id) {
                return "<script language='javascript'>
                        alert('Anda Tidak Memiliki Akses');
                        document.location='".asset('/')."';
                        </script>";
            }
        } else {
            return "<script language='javascript'>
                    alert('Anda Tidak Memiliki Akses');
                    document.location='".asset('/')."';
                    </script>";
        }

        $title = 'Sistem Informasi Pegawai Dinas Perumahan dan Permukiman Samarinda';
        $route = 'pangkat-golongan';
        $button = 'Tambah';

        return view('template.create', compact('title', 'button', 'route'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $modul_id = 38;
        $cek = Permission::where('modul_id', $modul_id)
                ->where('group_id', Auth::user()->group_id)->first();

        if ( $cek != "" ) {
            if ( $modul_id != $cek->modul_id) {
                return "<script language='javascript'>
                        alert('Anda Tidak Memiliki Akses');
                        document.location='".asset('/')."';
                        </script>";
            }
        } else {
            return "<script language='javascript'>
                    alert('Anda Tidak Memiliki Akses');
                    document.location='".asset('/')."';
                    </script>";
        }

        $this->validate($request, [
            'nama_golongan' => 'required|unique:pangkat_golongans',
            'keterangan_golongan' => 'required|unique:pangkat_golongans'
        ]);
        
        $pangkatGolongan = PangkatGolongan::firstOrCreate([
            'nama_golongan' => $request->nama_golongan,
            'keterangan_golongan' => $request->keterangan_golongan
        ]);
        
        
        Session::flash("flash_notif", [
            "level"     => "dismissible alert-success",
            "message"   => "Berhasil, menyimpan data Pangkat Golongan <strong>$pangkatGolongan->nama_golongan</strong>"
        ]);

        return \Redirect::route('pangkat-golongan.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PangkatGolongan  $pangkatGolongan
     * @return \Illuminate\Http\Response
     */
    public function show(PangkatGolongan $pangkatGolongan)
    {
        $modul_id = 39;
        $cek = Permission::where('modul_id', $modul_id)
                ->where('group_id', Auth::user()->group_id)->first();

        if ( $cek != "" ) {
            if ( $modul_id != $cek->modul_id) {
                return "<script language='javascript'>
                        alert('Anda Tidak Memiliki Akses');
                        document.location='".asset('/')."';
                        </script>";
            }
        } else {
            return "<script language='javascript'>
                    alert('Anda Tidak Memiliki Akses');
                    document.location='".asset('/')."';
                    </script>";
        }

        $data = $pangkatGolongan;
        $title = 'Sistem Informasi Pegawai Dinas Perumahan dan Permukiman Samarinda';
        $route = 'pangkat-golongan';
        
        return view('pangkat-golongan.show', compact('title', 'route', 'data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\PangkatGolongan  $pangkatGolongan
     * @return \Illuminate\Http\Response
     */
    public function edit(PangkatGolongan $pangkatGolongan)
    {
        $modul_id = 40;
        $cek = Permission::where('modul_id', $modul_id)
                ->where('group_id', Auth::user()->group_id)->first();

        if ( $cek != "" ) {
            if ( $modul_id != $cek->modul_id) {
                return "<script language='javascript'>
                        alert('Anda Tidak Memiliki Akses');
                        document.location='".asset('/')."';
                        </script>";
            }
        } else {
            return "<script language='javascript'>
                    alert('Anda Tidak Memiliki Akses');
                    document.location='".asset('/')."';
                    </script>";
        }

        $controller = 'PangkatGolonganController';
        $title = 'Sistem Informasi Pegawai Dinas Perumahan dan Permukiman Samarinda';
        $route = 'pangkat-golongan';
        $button = 'Perbarui';
        $data = $pangkatGolongan;
        
        return view('template.edit', compact('title', 'button', 'route', 'controller', 'data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\PangkatGolongan  $pangkatGolongan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PangkatGolongan $pangkatGolongan)
    {
        $modul_id = 40;
        $cek = Permission::where('modul_id', $modul_id)
                ->where('group_id', Auth::user()->group_id)->first();

        if ( $cek != "" ) {
            if ( $modul_id != $cek->modul_id) {
                return "<script language='javascript'>
                        alert('Anda Tidak Memiliki Akses');
                        document.location='".asset('/')."';
                        </script>";
            }
        } else {
            return "<script language='javascript'>
                    alert('Anda Tidak Memiliki Akses');
                    document.location='".asset('/')."';
                    </script>";
        }

        $this->validate($request, [
            'nama_golongan' => 'required|unique:pangkat_golongans,nama_golongan,'.$pangkatGolongan->id,
            'keterangan_golongan' => 'required|unique:pangkat_golongans,keterangan_golongan,'.$pangkatGolongan->id
        ]);
        
        $data = $pangkatGolongan;
        $data->nama_golongan = $request->nama_golongan;
        $data->keterangan_golongan = $request->keterangan_golongan;
        $data->update();

        Session::flash(
            "flash_notif", [
                "level"     => "dismissible alert-info",
                "message"   => "Berhasil, memperbarui data pangkat golongan <strong>$request->nama_golongan, $request->keterangan_golongan</strong>"
            ]);
        
        return \Redirect::route('pangkat-golongan.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PangkatGolongan  $pangkatGolongan
     * @return \Illuminate\Http\Response
     */
    public function destroy(PangkatGolongan $pangkatGolongan)
    {
        $modul_id = 41;
        $cek = Permission::where('modul_id', $modul_id)
                ->where('group_id', Auth::user()->group_id)->first();

        if ( $cek != "" ) {
            if ( $modul_id != $cek->modul_id) {
                return "<script language='javascript'>
                        alert('Anda Tidak Memiliki Akses');
                        document.location='".asset('/')."';
                        </script>";
            }
        } else {
            return "<script language='javascript'>
                    alert('Anda Tidak Memiliki Akses');
                    document.location='".asset('/')."';
                    </script>";
        }
        
        $data = $pangkatGolongan;
        $data->delete();
        Session::flash(
            "flash_notif",[
                "level"   => "dismissible alert-info",
                "massage" => "Berhasil Mengapus Data Pangkat Golongan <strong>$data->nama_golongan, $data->keterangan_golongan</strong>"
        ]);
        
        return \Redirect::route('pangkat-golongan.index');
    }
}
