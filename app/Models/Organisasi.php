<?php

namespace App\Models;

use App\Models\Pekerja;

use Illuminate\Database\Eloquent\Model;

class Organisasi extends Model
{
    protected $guarded = [];

    public function Pekerja()
    {
        return $this->belongsTo(Pekerja::class);
    }
    public function getTanggalMasukAttribute($tanggal_masuk)
    {
        \Carbon\Carbon::setLocale('id');
        return \Carbon\Carbon::parse($tanggal_masuk)
            ->format('d-F-Y');
    }
    public function getTanggalKeluarAttribute($tanggal_keluar)
    {
        \Carbon\Carbon::setLocale('id');
        return \Carbon\Carbon::parse($tanggal_keluar)
            ->format('d-F-Y');
    }
}
