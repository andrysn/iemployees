<?php

namespace App\Models;

use App\Models\Cuti;
use App\Models\Pekerja;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SuratCuti extends Model
{
    use SoftDeletes;
    protected $guarded = [];
    protected $dates = ['deleted_at'];

    public function Cuti()
    {
        return $this->belongsTo(Cuti::class);
    }
    public function Pekerja()
    {
        return $this->belongsTo(Pekerja::class);
    }
    public function getTanggalCutiAttribute($tanggal_cuti)
    {
        \Carbon\Carbon::setLocale('id');
        return \Carbon\Carbon::parse($tanggal_cuti)
            ->format('d-F-Y');
    }
    public function getTanggalKembaliAttribute($tanggal_kembali)
    {
        \Carbon\Carbon::setLocale('id');
        return \Carbon\Carbon::parse($tanggal_kembali)
            ->format('d-F-Y');
    }
}
