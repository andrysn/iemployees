<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Modul;
use App\Models\Group;

class Permission extends Model
{
    protected $guarded = [];

    public function Modul()
    {
        return $this->belongsTo(Modul::class);
    }
    public function Group()
    {
        return $this->belongsTo(Group::class);
    }
}
