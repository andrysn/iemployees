<?php

namespace App\Models;

use App\Models\Pekerja;

use Illuminate\Database\Eloquent\Model;

class Diklat extends Model
{
    protected $guarded = [];

    public function Pekerja()
    {
        return $this->belongsTo(Pekerja::class);
    }
    public function getTanggalDiklatAttribute($tanggal_diklat)
    {
        \Carbon\Carbon::setLocale('id');
        return \Carbon\Carbon::parse($tanggal_diklat)
            ->format('d-F-Y');
    }
}
