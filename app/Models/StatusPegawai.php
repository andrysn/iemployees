<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StatusPegawai extends Model
{
    use SoftDeletes;
    protected $guarded = [];
    protected $dates = ['deleted_at'];

    public function getDeletedAtAttribute($deleted_at)
    {
        is_null($deleted_at) ? $status = 'Aktif' : $status = 'Tidak Aktif';
        return $status;
    }
}
