<?php

namespace App\Models;

use App\Models\SuratCuti;

use Illuminate\Database\Eloquent\Model;

class Cuti extends Model
{
    protected $guarded = [];

    public function SuratCuti()
    {
        return $this->hasMany('SuratCuti');
    }
}
