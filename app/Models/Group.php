<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Permission;
use App\Models\User;

class Group extends Model
{
    protected $guarded = [];

    public function Permission()
    {
        return $this->hasMany('Permission');
    }

    public function User()
    {
        return $this->hasMany('User');
    }
}
