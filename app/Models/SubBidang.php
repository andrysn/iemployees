<?php

namespace App\Models;

use App\Models\Bidang;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SubBidang extends Model
{
    use SoftDeletes;
    protected $guarded = [];
    protected $dates = ['deleted_at'];

    public function Bidang()
    {
        return $this->belongsTo(Bidang::class);
    }
    public function getDeletedAtAttribute($deleted_at)
    {
        is_null($deleted_at) ? $status = 'Aktif' : $status = 'Tidak Aktif';
        return $status;
    }
}
