<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Permission;

class Modul extends Model
{
    public function Permission()
    {
        return $this->hasMany('Permission');
    }
}
