<?php

namespace App\Models;

use App\Models\Pekerja;

use Illuminate\Database\Eloquent\Model;

class Penghargaan extends Model
{
    protected $guarded = [];

    public function Pekerja()
    {
        return $this->belongsTo(Pekerja::class);
    }
    public function getTanggalPenghargaanAttribute($tanggal_penghargaan)
    {
        \Carbon\Carbon::setLocale('id');
        return \Carbon\Carbon::parse($tanggal_penghargaan)
            ->format('d-F-Y');
    }
}
