<?php

namespace App\Models;

use App\Models\Cuti;
use App\Models\Diklat;
use App\Models\Bidang;
use App\Models\Kredit;
use App\Models\Jabatan;
use App\Models\Promosi;
use App\Models\Keluarga;
use App\Models\SuratCuti;
use App\Models\Pendidikan;
use App\Models\Organisasi;
use App\Models\Penghargaan;
use App\Models\RiwayatKerja;
use App\Models\StatusPegawai;
use App\Models\PangkatEselon;
use App\Models\PangkatGolongan;

use Illuminate\Database\Eloquent\Model;

class Pekerja extends Model
{
    protected $guarded = [];

    /**
     * variable static untuk agama
     */
    public static $ISLAM = '1';

    /**
     * variable static untuk agama
     */
    public static $KRISTEN = '2';

    /**
     * variable static untuk agama
     */
    public static $KATOLIK = '3';

    /**
     * variable static untuk agama
     */
    public static $BUDHA = '4';

    /**
     * variable static untuk agama
     */
    public static $HINDU = '5';

    /**
     * variable static untuk agama
     */
    public static $KONGHUCU = '6';

    public function Bidang()
    {
        return $this->belongsTo(Bidang::class);
    }
    public function Jabatan()
    {
        return $this->belongsTo(Jabatan::class);
    }
    public function PangkatEselon()
    {
        return $this->belongsTo(PangkatEselon::class);
    }
    public function PangkatGolongan()
    {
        return $this->belongsTo(PangkatGolongan::class);
    }
    public function StatusPegawai()
    {
        return $this->belongsTo(StatusPegawai::class);
    }
    public function Pendidikan()
    {
        return $this->hasMany('Pendidikan');
    }
    public function Organisasi()
    {
        return $this->hasMany('Organisasi');
    }
    public function Diklat()
    {
        return $this->hasMany('Diklat');
    }
    public function Promosi()
    {
        return $this->hasMany('Promosi');
    }
    public function RiwayatKerja()
    {
        return $this->hasMany('RiwayatKerja');
    }
    public function Keluarga()
    {
        return $this->hasMany('Keluarga');
    }
    public function Kredit()
    {
        return $this->hasMany('Kredit');
    }
    public function SuratCuti()
    {
        return $this->hasMany('SuratCuti');
    }
    public function Penghargaan()
    {
        return $this->hasMany('Penghargaan');
    }
    public function getStatusAttribute($status)
    {
        $status == '1' ? $status = 'Aktif' : $status = 'Tidak Aktif';
        return $status;
    }
    public function getJenisKelaminAttribute($jenisKelamin)
    {
        $jenisKelamin == '1' ? $jenisKelamin = 'Laki-laki' : $jenisKelamin = 'Perempuan';
        return $jenisKelamin;
    }
    public function getFaithAttribute()
    {
        $label = [
            Pekerja::$ISLAM => 'Islam',
            Pekerja::$KRISTEN => 'Kristen',
            Pekerja::$KATOLIK => 'Katolik',
            Pekerja::$BUDHA => 'Budha',
            Pekerja::$HINDU => 'Hindu',
            Pekerja::$KONGHUCU => 'Konghucu'
        ];
        return $label[$this->agama];
    }
    public function getTanggalLahirAttribute($tanggal_lahir)
    {
        \Carbon\Carbon::setLocale('id');
        return \Carbon\Carbon::parse($tanggal_lahir)
            ->format('d-F-Y');
    }
}
