<?php

namespace App\Models;

use App\Models\Pekerja;

use Illuminate\Database\Eloquent\Model;

class Pendidikan extends Model
{
    protected $guarded = [];

    public function Pekerja()
    {
        return $this->belongsTo(Pekerja::class);
    }
}
