<?php

namespace App\Models;

use App\Models\Pekerja;
use App\Models\SubBidang;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Bidang extends Model
{
    use SoftDeletes;
    protected $guarded = [];
    protected $dates = ['deleted_at'];

    public function SubBidang()
    {
        return $this->hasMany('SubBidang');
    }
    public function Pekerja()
    {
        return $this->hasMany('Pekerja');
    }
    public function getDeletedAtAttribute($deleted_at)
    {
        is_null($deleted_at) ? $status = 'Aktif' : $status = 'Tidak Aktif';
        return $status;
    }
}
