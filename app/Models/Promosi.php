<?php

namespace App\Models;

use App\Models\Pekerja;

use Illuminate\Database\Eloquent\Model;

class Promosi extends Model
{
    protected $guarded = [];

    public function Pekerja()
    {
        return $this->belongsTo(Pekerja::class);
    }
}
