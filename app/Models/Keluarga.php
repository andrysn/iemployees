<?php

namespace App\Models;

use App\Models\Pekerja;

use Illuminate\Database\Eloquent\Model;

class Keluarga extends Model
{
    protected $guarded = [];

    /**
     * variable static untuk agama
     */
    public static $ISLAM = '1';

    /**
     * variable static untuk agama
     */
    public static $KRISTEN = '2';

    /**
     * variable static untuk agama
     */
    public static $KATOLIK = '3';

    /**
     * variable static untuk agama
     */
    public static $BUDHA = '4';

    /**
     * variable static untuk agama
     */
    public static $HINDU = '5';
    
    /**
     * variable static untuk agama
     */
    public static $KONGHUCU = '6';
    
    public function Pekerja()
    {
        return $this->belongsTo(Pekerja::class);
    }
    public function getFaithAttribute()
    {
        $label = [
            Pekerja::$ISLAM => 'Islam',
            Pekerja::$KRISTEN => 'Kristen',
            Pekerja::$KATOLIK => 'Katolik',
            Pekerja::$BUDHA => 'Budha',
            Pekerja::$HINDU => 'Hindu',
            Pekerja::$KONGHUCU => 'Konghucu'
        ];
        return $label[$this->agama];
    }
    public function getJenisKelaminAttribute($jenisKelamin)
    {
        $jenisKelamin == '1' ? $jenisKelamin = 'Laki-laki' : $jenisKelamin = 'Perempuan';
        return $jenisKelamin;
    }
    public function getTanggalLahirAttribute($tanggal_lahir)
    {
        \Carbon\Carbon::setLocale('id');
        return \Carbon\Carbon::parse($tanggal_lahir)
            ->format('d-F-Y');
    }
}
