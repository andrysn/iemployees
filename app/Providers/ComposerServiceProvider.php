<?php

namespace App\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function boot()
    {
        // Using class based composers...
        View::composer(
            ['home', 'welcome'], 'App\Http\ViewComposers\ProfileComposer'
        );

        View::composer(
            'latest-post', 'App\Http\ViewComposers\LatestPostComposer'
        );

        // Using Closure based composers...
        View::composer(['dashboard'], function ($view) {
            // $view->with('count', $this->users->count());
        });
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}