<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePekerjasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pekerjas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('bidang_id')->nullable()->unsigned()->index();
            $table->integer('status_pegawai_id')->unsigned()->index();
            $table->integer('jabatan_id')->unsigned()->index();
            $table->integer('pangkat_golongan_id')->nullable()->unsigned()->index();
            $table->integer('pangkat_eselon_id')->nullable()->unsigned()->index();
            $table->string('nip', 100);
            $table->string('foto', 100);
            $table->string('keterangan_pegawai', 100);
            $table->string('nama_pegawai', 100);
            $table->string('alamat', 100);
            $table->string('tempat_lahir', 100);
            $table->date('tanggal_lahir');
            $table->enum('jenis_kelamin', ['1', '2']); // 1 Laki-laki, 2 Perempuan
            $table->enum('agama', ['1', '2', '3', '4', '5', '6']); // 1 Islam, 2 Kristen, 3 Katolik, 4 Hindu, 5 Budha, 6 Lainnya
            $table->string('nomor_karpeg', 100);
            $table->string('nomor_bpjs', 100);
            $table->string('nomor_pokok_wajib_pajak', 100);
            $table->enum('status', ['1', '2'])->default(1); // 1 aktif, 2 non-aktif
            $table->timestamps();

            $table->foreign('bidang_id')
            ->references('id')
            ->on('bidangs')
            ->onDelete('cascade')
            ->onUpdate('cascade');

            $table->foreign('status_pegawai_id')
            ->references('id')
            ->on('status_pegawais')
            ->onDelete('cascade')
            ->onUpdate('cascade');

            $table->foreign('jabatan_id')
            ->references('id')
            ->on('jabatans')
            ->onDelete('cascade')
            ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pekerjas');
    }
}
