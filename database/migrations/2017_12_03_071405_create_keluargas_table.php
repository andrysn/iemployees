<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKeluargasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('keluargas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pekerja_id')->unsigned()->index();
            $table->string('nama_lengkap', 100);
            $table->string('hubungan_keluarga', 100);
            $table->string('pekerjaan', 100)->nullable();
            $table->enum('agama', ['1', '2', '3', '4', '5', '6']); // 1 Islam, 2 Kristen, 3 Katolik, 4 Hindu, 5 Budha, 6 Lainnya
            $table->string('nik', 100);
            $table->string('tempat_lahir', 100);
            $table->enum('jenis_kelamin', ['1', '2']); // 1 Laki-laki, 2 Perempuan
            $table->date('tanggal_lahir');
            $table->timestamps();

            $table->foreign('pekerja_id')
            ->references('id')
            ->on('pekerjas')
            ->onDelete('cascade')
            ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('keluargas');
    }
}
