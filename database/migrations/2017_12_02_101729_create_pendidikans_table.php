<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePendidikansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pendidikans', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pekerja_id')->unsigned()->index();
            $table->string('jenjang', 100);
            $table->string('nama_akademi', 100);
            $table->string('nomor_ijazah', 100);
            $table->string('foto_ijazah', 50);
            $table->timestamps();

            $table->foreign('pekerja_id')
            ->references('id')
            ->on('pekerjas')
            ->onDelete('cascade')
            ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pendidikans');
    }
}
