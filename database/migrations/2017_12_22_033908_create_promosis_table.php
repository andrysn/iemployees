<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePromosisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('promosis', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pekerja_id')->unsigned()->index();
            $table->string('jabatan_lama', 100);
            $table->string('jabatan_baru', 100);
            $table->string('penetap', 100);
            $table->string('nomor_sk', 100);
            $table->string('foto_sk', 50);
            $table->date('tanggal_sk');
            $table->timestamps();

            $table->foreign('pekerja_id')
            ->references('id')
            ->on('pekerjas')
            ->onDelete('cascade')
            ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('promosis');
    }
}
