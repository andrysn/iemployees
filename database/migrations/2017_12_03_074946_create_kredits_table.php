<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKreditsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kredits', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pekerja_id')->unsigned()->index();
            $table->string('penetapan', 100);
            $table->string('angka_kredit_lama', 100);
            $table->string('angka_kredit_baru', 100);
            $table->string('jumlah', 100);
            $table->timestamps();

            $table->foreign('pekerja_id')
            ->references('id')
            ->on('pekerjas')
            ->onDelete('cascade')
            ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kredits');
    }
}
