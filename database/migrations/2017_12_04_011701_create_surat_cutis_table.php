<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSuratCutisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('surat_cutis', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pekerja_id')->unsigned()->index();
            $table->integer('cuti_id')->unsigned()->index();
            $table->date('tanggal_cuti');
            $table->date('tanggal_kembali');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('pekerja_id')
            ->references('id')
            ->on('pekerjas')
            ->onDelete('cascade')
            ->onUpdate('cascade');

            $table->foreign('cuti_id')
            ->references('id')
            ->on('cutis')
            ->onDelete('cascade')
            ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('surat_cutis');
    }
}