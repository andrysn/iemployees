<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubBidangsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sub_bidangs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('bidang_id')->nullable()->unsigned()->index();
            $table->string('nama_sub_bidang', 100);
            $table->string('kode_sub_bidang', 100);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('bidang_id')
            ->references('id')
            ->on('bidangs')
            ->onDelete('cascade')
            ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sub_bidangs');
    }
}
