<?php

use Illuminate\Database\Seeder;

use App\Models\User;
use App\Models\Modul;
use App\Models\Group;

class InitDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->ModulSeeder();
        $this->GroupSeeder();
        $this->UserSeeder();
    }
    public function ModulSeeder()
    {
        $data = [
            // Modul Bidang
            [ 'nama_modul' => 'Bidang Index' ],[ 'nama_modul' => 'Bidang Tambah' ],[ 'nama_modul' => 'Bidang Detail' ],[ 'nama_modul' => 'Bidang Ubah' ],[ 'nama_modul' => 'Bidang Hapus' ],
            // Modul Cuti
            [ 'nama_modul' => 'Cuti Index' ],[ 'nama_modul' => 'Cuti Tambah' ],[ 'nama_modul' => 'Cuti Detail' ],[ 'nama_modul' => 'Cuti Ubah' ],[ 'nama_modul' => 'Cuti Hapus' ],
            // Modul Diklat
            [ 'nama_modul' => 'Diklat Tambah' ],[ 'nama_modul' => 'Diklat Detail' ],[ 'nama_modul' => 'Diklat Hapus' ],
            // Modul Group
            [ 'nama_modul' => 'Group Index' ],[ 'nama_modul' => 'Group Tambah' ],[ 'nama_modul' => 'Group Ubah' ],[ 'nama_modul' => 'Group Hapus' ],
            // Modul Jabatan
            [ 'nama_modul' => 'Jabatan Index' ],[ 'nama_modul' => 'Jabatan Tambah' ],[ 'nama_modul' => 'Jabatan Detail' ],[ 'nama_modul' => 'Jabatan Ubah' ],[ 'nama_modul' => 'Jabatan Hapus' ],
            // Modul Keluarga
            [ 'nama_modul' => 'Keluarga Tambah' ],[ 'nama_modul' => 'Keluarga Detail' ],[ 'nama_modul' => 'Keluarga Hapus' ],
            // Modul Kredit
            [ 'nama_modul' => 'Kredit Tambah' ],[ 'nama_modul' => 'Kredit Detail' ],[ 'nama_modul' => 'Kredit Hapus' ],
            // Modul Organisasi
            [ 'nama_modul' => 'Organisasi Tambah' ],[ 'nama_modul' => 'Organisasi Detail' ],[ 'nama_modul' => 'Organisasi Hapus' ],
            // Modul Jabatan
            [ 'nama_modul' => 'Eselon Index' ],[ 'nama_modul' => 'Eselon Tambah' ],[ 'nama_modul' => 'Eselon Detail' ],[ 'nama_modul' => 'Eselon Ubah' ],[ 'nama_modul' => 'Eselon Hapus' ],
            // Modul Golongan
            [ 'nama_modul' => 'Golongan Index' ],[ 'nama_modul' => 'Golongan Tambah' ],[ 'nama_modul' => 'Golongan Detail' ],[ 'nama_modul' => 'Golongan Ubah' ],[ 'nama_modul' => 'Golongan Hapus' ],
            // Modul Pekerja
            [ 'nama_modul' => 'Pekerja Index' ],[ 'nama_modul' => 'Pekerja Tambah' ],[ 'nama_modul' => 'Pekerja Detail' ],[ 'nama_modul' => 'Pekerja Ubah' ],[ 'nama_modul' => 'Pekerja Hapus' ],
            // Modul Pendidikan
            [ 'nama_modul' => 'Pendidikan Tambah' ],[ 'nama_modul' => 'Pendidikan Detail' ],[ 'nama_modul' => 'Pendidikan Hapus' ],
            // Modul Penghargaan
            [ 'nama_modul' => 'Penghargaan Tambah' ],[ 'nama_modul' => 'Penghargaan Detail' ],[ 'nama_modul' => 'Penghargaan Hapus' ],
            // Modul Riwayat Kerja
            [ 'nama_modul' => 'Riwayat Kerja Tambah' ],[ 'nama_modul' => 'Riwayat Kerja Detail' ],[ 'nama_modul' => 'Riwayat Kerja Hapus' ],
            // Modul Status Pegawai
            [ 'nama_modul' => 'Status Pegawai Index' ],[ 'nama_modul' => 'Status Pegawai Tambah' ],[ 'nama_modul' => 'Status Pegawai Detail' ],[ 'nama_modul' => 'Status Pegawai Ubah' ],[ 'nama_modul' => 'Status Pegawai Hapus' ],
            // Modul Sub Bidang
            [ 'nama_modul' => 'Sub Bidang Index' ],[ 'nama_modul' => 'Sub Bidang Tambah' ],[ 'nama_modul' => 'Sub Bidang Detail' ],[ 'nama_modul' => 'Sub Bidang Ubah' ],[ 'nama_modul' => 'Sub Bidang Hapus' ],
            // Modul Surat Cuti
            [ 'nama_modul' => 'Surat Cuti Index' ],[ 'nama_modul' => 'Surat Cuti Tambah' ],[ 'nama_modul' => 'Surat Cuti Detail' ],[ 'nama_modul' => 'Surat Cuti Ubah' ],[ 'nama_modul' => 'Surat Cuti Hapus' ],
            // Modul User
            [ 'nama_modul' => 'User Index' ],[ 'nama_modul' => 'User Tambah' ],[ 'nama_modul' => 'User Ubah' ],[ 'nama_modul' => 'User Hapus' ],
            // Modul Promosi
            [ 'nama_modul' => 'Promosi Tambah' ],[ 'nama_modul' => 'Promosi Detail' ],[ 'nama_modul' => 'Promosi Hapus' ],
        ];
        
        foreach($data as $modul){
            Modul::create($modul);
        }
    }
    public function GroupSeeder()
    {
        $data = [
            [
                'id'            => '1',
                'nama_group'     => 'Admin'
            ],
            [
                'id'            => '2',
                'nama_group'     => 'Operator'
            ]
        ];
        
        foreach($data as $group){
            Group::create($group);
        }
    }
    public function UserSeeder()
    {
        $data = [
            [
                'name'     => 'Admin',
                'email'    => 'admin@gmail.com',
                'level'    => 'admin',
                'password' => bcrypt('admin123'),
                'group_id' => '1'
            ],
            [
                'name'     => 'User',
                'email'    => 'user@gmail.com',
                'level'    => 'user',
                'password' => bcrypt('user123'),
                'group_id' => '2'
            ]
        ];
        
        foreach($data as $user){
            User::create($user);
        }
    }

}
