<div class="btn-toolbar" role="toolbar">

        <div class="btn-employees" role="employees">
            {!! Form::open(['route' => [$route.'.destroy', $data->id], 'method' => 'delete', 'class' => 'form-inline', 'id' => "delete-form"]) !!}
                {!! link_to_route($route.'.show', 'Detail', $data->id, ['class' => 'btn btn-warning btn-sm'] ) !!}
                {!! link_to_route($route.'.edit', 'Ubah', $data->id, ['class' => 'btn btn-primary btn-sm'] ) !!}
                {!! Form::submit('Hapus', ['class' => 'btn btn-danger btn-sm', 'onclick' => "return confirmation();"]) !!}

            {!! Form::close() !!}
        </div>

</div>