@extends('layouts.admin')

@section('content')

<div class="row">
	<div class="col-md-12">
		<div class="box-widget widget-module">
			@include(
				'template.tambah'
			)

			<div class="widget-container">
				<div class="widget-block">
					<table id="users-table" class="table table-hover">
					<thead>
					<tr>
						<th>
							No
						</th>
						<th>
							Bidang
						</th>
						<th>
							Nama Sub Bidang
						</th>
						<th>
							Keterangan Sub Bidang
						</th>
						<th>
							Status
						</th>
						<th>
							Action
						</th>
					</tr>
					</thead>
					<tbody>
						@foreach ($subBidang as $index => $data)
						<tr>
						<td>
							{{ $index + 1 + ( ( $subBidang->CurrentPage() - 1 ) * $subBidang->PerPage() ) }}
						</td>
						<td>
							{{ $data->bidang->nama_bidang }}
						</td>
						<td>
							{{ $data->nama_sub_bidang }}
						</td>
						<td>
							{{ $data->kode_sub_bidang }}
						</td>
						<td>
							{{ $data->deleted_at }}
						</td>
						<td class="tc-center">
							@include(
								'template.aksi'
							)
						</td>
					</tr>
		            @endforeach
        			</tbody>
		  			</table>
					
				</div>
			</div>
		</div>
	</div>

</div>


@endsection
