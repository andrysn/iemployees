<div class="form-group{{ $errors->has('bidang_id') ? ' has-error' : '' }}">
    {{ Form::label('nama_bidang', null, ['class' => 'col-sm-2 control-label']) }}
    <div class="col-md-6">
        {{ Form::select('bidang_id', $bidang, null, ['class' => 'form-control selectpicker', 'placeholder' => 'Pilih bidang...', 'data-live-search' => 'true']) }}
        @if($errors->has('bidang_id'))
            <span class="help-block">
                <strong>{{ $errors->first('bidang_id') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('nama_sub_bidang') ? ' has-error' : '' }}">
    {{ Form::label('nama_sub_bidang', null, ['class' =>'col-sm-2 control-label']) }}
    <div class=" col-md-6">
        {{ Form::text('nama_sub_bidang', null,['class' =>'form-control', 'placeholder'=> 'Nama Sub Bidang']) }} 
        @if ($errors->has('nama_sub_bidang'))
        <span class="help-block">
            <strong>{{ $errors->first('nama_sub_bidang') }}</strong>
        </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('kode_sub_bidang') ? ' has-error' : '' }}">
    {{ Form::label('keterangan_sub_bidang', null, ['class' =>'col-sm-2 control-label']) }}
    <div class=" col-md-6">
        {{ Form::text('kode_sub_bidang', null,['class' =>'form-control', 'placeholder'=> 'Keterangan Sub Bidang']) }} 
        @if ($errors->has('kode_sub_bidang'))
        <span class="help-block">
            <strong>{{ $errors->first('kode_sub_bidang') }}</strong>
        </span>
        @endif
    </div>
</div>

@include(
    'template.button-form'
)