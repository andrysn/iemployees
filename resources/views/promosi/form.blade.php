{{ Form::hidden('pekerja_id', Request::segment(2)) }}

<div class="form-group{{ $errors->has('penetap') ? ' has-error' : '' }}">
    {{ Form::label('pejabat_yang_menentapkan', null, ['class' =>'col-sm-2 control-label']) }}
    <div class=" col-md-6">
        {{ Form::text('penetap', null,['class' =>'form-control', 'placeholder'=> 'Pejabat Yang Menetapkan']) }} 
        @if ($errors->has('penetap'))
        <span class="help-block">
            <strong>{{ $errors->first('penetap') }}</strong>
        </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('nomor_sk') ? ' has-error' : '' }}">
    {{ Form::label('nomor_sk', null, ['class' =>'col-sm-2 control-label']) }}
    <div class=" col-md-6">
        {{ Form::text('nomor_sk', null,['class' =>'form-control', 'placeholder'=> 'Nomor SK']) }} 
        @if ($errors->has('nomor_sk'))
        <span class="help-block">
            <strong>{{ $errors->first('nomor_sk') }}</strong>
        </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('pangkat_lama') ? ' has-error' : '' }}">
    {{ Form::label('pangkat_terakhir', null, ['class' =>'col-sm-2 control-label']) }}
    <div class=" col-md-6">
        {{ Form::text('pangkat_lama', null,['class' =>'form-control', 'placeholder'=> 'Pangkat Terakhir']) }} 
        @if ($errors->has('pangkat_lama'))
        <span class="help-block">
            <strong>{{ $errors->first('pangkat_lama') }}</strong>
        </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('pangkat_baru') ? ' has-error' : '' }}">
    {{ Form::label('pangkat_sekarang', null, ['class' =>'col-sm-2 control-label']) }}
    <div class=" col-md-6">
        {{ Form::text('pangkat_baru', null,['class' =>'form-control', 'placeholder'=> 'Pangkat sekarang']) }} 
        @if ($errors->has('pangkat_baru'))
        <span class="help-block">
            <strong>{{ $errors->first('pangkat_baru') }}</strong>
        </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('foto_sk') ? ' has-error' : '' }}">
    {{Form::label('foto_sk', null, ['class'=>'col-sm-2 control-label']) }}
    <div class=" col-md-6">
        {{ Form::file('foto_sk') }}
        <span>Type JPEG, JPG, PNG</span>
        @if (isset($data))
            <p>{!! Html::image(asset('asset/foto-sk/'.$data->foto_sk), null, ['class'=> 'img-rounded img-responsive' , 'width' => '40%']) !!}</p>
        @endif
        @if($errors->has('foto_sk'))
        <span class="help-block">
            <strong>{{ $errors->first('foto_sk') }}</strong>
        </span>
        @endif 
    </div> 
</div>

<div class="form-group{{ $errors->has('tanggal_sk') ? ' has-error' : '' }}">
    {{ Form::label('tanggal_sk', null,['class' =>'col-sm-2 control-label'])}}
    <div class=" col-md-6">
        {{ Form::date('tanggal_sk', null,['class' =>'form-control', 'placeholder'=> 'Tanggal SK']) }} 
        @if ($errors->has('tanggal_sk'))
        <span class="help-block">
            <strong>{{ $errors->first('tanggal_sk')}}</strong>
        </span>
        @endif 
    </div>
</div>
                
<div class="form-group">
    <div class="col-sm-8 col-sm-offset-4">
        {!! Form::submit($button, ['class' => 'btn btn-primary']) !!}
    </div>
</div>