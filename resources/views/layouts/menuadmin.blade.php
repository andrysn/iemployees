<div class="left-navigation">
    <ul class="list-accordion">
        <li><a href="{{ url('/') }}" class="waves-effect"><span class="nav-icon"><i class="fa fa-home"></i></span><span class="nav-label">Dashboard</span></a>

        </li>
        

        <li><a href="#" class="waves-effect"><span class="nav-icon"><i class="fa fa-group"></i></span><span class="nav-label">Master Pegawai</span></a>
        </span></a>
            <ul>

                <li><a href="{{ url('pekerja') }}">Data Pegawai</a></li>
                
                <li><a href="{{ url('surat-cuti') }}">Surat Cuti</a></li>
                
            </ul>
        </li>
        
        <li><a href="{{ url('master') }}" class="waves-effect"><span class="nav-icon"><i class="fa fa-gear"></i></span><span class="nav-label">Master Data</span></a>
        </span></a>
            <ul>

                <li><a href="{{ url('jabatan') }}">Jabatan</a></li>
                
                <li><a href="{{ url('bidang') }}">Bidang</a></li>

                <li><a href="{{ url('sub-bidang') }}">Sub Bidang</a></li>

                <li><a href="{{ url('cuti') }}">Cuti</a></li>

                <li><a href="{{ url('status-pegawai') }}">Status Pegawai</a></li>

                <li><a href="{{ url('pangkat-golongan') }}">Pangkat Golongan</a></li>

                <li><a href="{{ url('pangkat-eselon') }}">Pangkat Eselon</a></li>
                
            </ul>
        </li>
    </ul>
</div>
