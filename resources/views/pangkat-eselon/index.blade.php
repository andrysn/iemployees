@extends('layouts.admin')

@section('content')

<div class="row">
	<div class="col-md-12">
		<div class="box-widget widget-module">
			@include(
				'template.tambah'
			)

			<div class="widget-container">
				<div class="widget-block">
					<table id="users-table" class="table table-hover">
					<thead>
					<tr>
						<th>
							No
						</th>
						<th>
							Tingkat Eselon
						</th>
						<th>
							Keterangan Pangkat Eselon
						</th>
						<th>
							Status
						</th>
						<th>
							Aksi
						</th>
					</tr>
					</thead>
					<tbody>
						@foreach ($pangkatEselon as $index => $data)
						<tr>
						<td>
							{{ $index + 1 + ( ( $pangkatEselon->CurrentPage() - 1 ) * $pangkatEselon->PerPage() ) }}
						</td>
						<td>
							{{ $data->nama_eselon }}
						</td>
						<td>
							{{ $data->keterangan_eselon }}
						</td>
						<td>
							{{ $data->deleted_at }}
						</td>
						<td class="tc-center">
							@include(
								'template.aksi'
							)
						</td>
					</tr>
		            @endforeach
        			</tbody>
		  			</table>
					
				</div>
			</div>
		</div>
	</div>

</div>


@endsection
