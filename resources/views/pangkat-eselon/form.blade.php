<div class="form-group{{ $errors->has('nama_eselon') ? ' has-error' : '' }}">
    {{ Form::label('tingkat_eselon', null, ['class' =>'col-sm-2 control-label']) }}
    <div class=" col-md-6">
        {{ Form::text('nama_eselon', null,['class' =>'form-control', 'placeholder'=> 'ex. III.b']) }} 
        @if ($errors->has('nama_eselon'))
        <span class="help-block">
            <strong>{{ $errors->first('nama_eselon') }}</strong>
        </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('keterangan_eselon') ? ' has-error' : '' }}">
    {{ Form::label('keterangan_eselon', null, ['class' =>'col-sm-2 control-label']) }}
    <div class=" col-md-6">
        {{ Form::text('keterangan_eselon', null,['class' =>'form-control', 'placeholder'=> 'ex. Kepala Balai']) }} 
        @if ($errors->has('keterangan_eselon'))
        <span class="help-block">
            <strong>{{ $errors->first('keterangan_eselon') }}</strong>
        </span>
        @endif
    </div>
</div>

@include(
    'template.button-form'
)