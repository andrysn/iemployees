{{ Form::hidden('pekerja_id', Request::segment(2)) }}

<div class="form-group{{ $errors->has('nama_kantor') ? ' has-error' : '' }}">
    {{ Form::label('nama_kantor', null, ['class' =>'col-sm-2 control-label']) }}
    <div class=" col-md-6">
        {{ Form::text('nama_kantor', null,['class' =>'form-control', 'placeholder'=> 'Nama Kantor']) }} 
        @if ($errors->has('nama_kantor'))
        <span class="help-block">
            <strong>{{ $errors->first('nama_kantor') }}</strong>
        </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('jabatan') ? ' has-error' : '' }}">
    {{ Form::label('jabatan', null, ['class' =>'col-sm-2 control-label']) }}
    <div class=" col-md-6">
        {{ Form::text('jabatan', null,['class' =>'form-control', 'placeholder'=> 'Jabatan']) }} 
        @if ($errors->has('jabatan'))
        <span class="help-block">
            <strong>{{ $errors->first('jabatan') }}</strong>
        </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('keterangan') ? ' has-error' : '' }}">
    {{ Form::label('keterangan', null, ['class' =>'col-sm-2 control-label']) }}
    <div class=" col-md-6">
        {{ Form::text('keterangan', null,['class' =>'form-control', 'placeholder'=> 'Keterangan']) }} 
        @if ($errors->has('keterangan'))
        <span class="help-block">
            <strong>{{ $errors->first('keterangan') }}</strong>
        </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('tanggal_masuk') ? ' has-error' : '' }}">
    {{ Form::label('tanggal_masuk', null,['class' =>'col-sm-2 control-label'])}}
    <div class=" col-md-6">
        {{ Form::date('tanggal_masuk', null,['class' =>'form-control', 'placeholder'=> 'Tanggal Masuk']) }} 
        @if ($errors->has('tanggal_masuk'))
        <span class="help-block">
            <strong>{{ $errors->first('tanggal_masuk')}}</strong>
        </span>
        @endif 
    </div>
</div>

<div class="form-group{{ $errors->has('tanggal_keluar') ? ' has-error' : '' }}">
    {{ Form::label('tanggal_keluar', null,['class' =>'col-sm-2 control-label'])}}
    <div class=" col-md-6">
        {{ Form::date('tanggal_keluar', null,['class' =>'form-control', 'placeholder'=> 'Tanggal Masuk']) }} 
        @if ($errors->has('tanggal_keluar'))
        <span class="help-block">
            <strong>{{ $errors->first('tanggal_keluar')}}</strong>
        </span>
        @endif 
    </div>
</div>
                
<div class="form-group">
    <div class="col-sm-8 col-sm-offset-4">
        {!! Form::submit($button, ['class' => 'btn btn-primary']) !!}
    </div>
</div>