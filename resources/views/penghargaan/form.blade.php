{{ Form::hidden('pekerja_id', Request::segment(2)) }}

<div class="form-group{{ $errors->has('nama_penghargaan') ? ' has-error' : '' }}">
    {{ Form::label('nama_penghargaan', null, ['class' =>'col-sm-2 control-label']) }}
    <div class=" col-md-6">
        {{ Form::text('nama_penghargaan', null,['class' =>'form-control', 'placeholder'=> 'Nama penghargaan']) }} 
        @if ($errors->has('nama_penghargaan'))
        <span class="help-block">
            <strong>{{ $errors->first('nama_penghargaan') }}</strong>
        </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('instansi_pemberi') ? ' has-error' : '' }}">
    {{ Form::label('instansi_pemberi', null, ['class' =>'col-sm-2 control-label']) }}
    <div class=" col-md-6">
        {{ Form::text('instansi_pemberi', null,['class' =>'form-control', 'placeholder'=> 'Instansi yang menyerahkan']) }} 
        @if ($errors->has('instansi_pemberi'))
        <span class="help-block">
            <strong>{{ $errors->first('instansi_pemberi') }}</strong>
        </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('foto_trophy') ? ' has-error' : '' }}">
    {{Form::label('foto_penghargaan', null, ['class'=>'col-sm-2 control-label']) }}
    <div class=" col-md-6">
        {{ Form::file('foto_trophy') }}
        <span>Type JPEG, JPG, PNG</span>
        @if (isset($data))
            <p>{!! Html::image(asset('asset/foto-trophy/'.$data->foto_trophy), null, ['class'=> 'img-rounded img-responsive' , 'width' => '40%']) !!}</p>
        @endif
        @if($errors->has('foto_trophy'))
        <span class="help-block">
            <strong>{{ $errors->first('foto_trophy') }}</strong>
        </span>
        @endif 
    </div> 
</div>

<div class="form-group{{ $errors->has('tanggal_penghargaan') ? ' has-error' : '' }}">
    {{ Form::label('tanggal_penghargaan', null,['class' =>'col-sm-2 control-label'])}}
    <div class=" col-md-6">
        {{ Form::date('tanggal_penghargaan', null,['class' =>'form-control', 'placeholder'=> 'Tanggal penghargaan']) }} 
        @if ($errors->has('tanggal_penghargaan'))
        <span class="help-block">
            <strong>{{ $errors->first('tanggal_penghargaan')}}</strong>
        </span>
        @endif 
    </div>
</div>
                
<div class="form-group">
    <div class="col-sm-8 col-sm-offset-4">
        {!! Form::submit($button, ['class' => 'btn btn-primary']) !!}
    </div>
</div>