@extends('layouts.admin')

@section('content')
 
    <div class="row">
        <div class="col-md-12">
            <div class="box-widget widget-module">
                <div class="widget-head clearfix">
                    <span class="h-icon"><i class="fa fa-bars"></i></span>
                    <h4>Detail Data</h4>
                </div>
                
                <div class="widget-container">
                    <div class=" widget-block">

                        <label class="col-md-3 control-label">Pangkat Golongan</label>
                        <dl>
                            <dt>{{ $data->nama_golongan }}</dt>
                        </dl>

                        <label class="col-md-3 control-label">Keterangan Golongan</label>
                        <dl>
                            <dt>{{ $data->keterangan_golongan }}</dt>
                        </dl>

                        <label class="col-md-3 control-label">Status</label>
                        <dl>
                            <dt>{{ $data->deleted_at }}</dt>
                        </dl>

                        @include('template.kembali')

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection