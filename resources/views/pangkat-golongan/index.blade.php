@extends('layouts.admin')

@section('content')

<div class="row">
	<div class="col-md-12">
		<div class="box-widget widget-module">
			@include(
				'template.tambah'
			)

			<div class="widget-container">
				<div class="widget-block">
					<table id="users-table" class="table table-hover">
					<thead>
					<tr>
						<th>
							No
						</th>
						<th>
							Pangkat Golongan
						</th>
						<th>
							Keterangan Pangkat Golongan
						</th>
						<th>
							Status
						</th>
						<th>
							Aksi
						</th>
					</tr>
					</thead>
					<tbody>
						@foreach ($pangkatGolongan as $index => $data)
						<tr>
						<td>
							{{ $index + 1 + ( ( $pangkatGolongan->CurrentPage() - 1 ) * $pangkatGolongan->PerPage() ) }}
						</td>
						<td>
							{{ $data->nama_golongan }}
						</td>
						<td>
							{{ $data->keterangan_golongan }}
						</td>
						<td>
							{{ $data->deleted_at }}
						</td>
						<td class="tc-center">
							@include(
								'template.aksi'
							)
						</td>
					</tr>
		            @endforeach
        			</tbody>
		  			</table>
					
				</div>
			</div>
		</div>
	</div>

</div>


@endsection
