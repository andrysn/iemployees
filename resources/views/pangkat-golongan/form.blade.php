<div class="form-group{{ $errors->has('nama_golongan') ? ' has-error' : '' }}">
    {{ Form::label('nama_golongan', null, ['class' =>'col-sm-2 control-label']) }}
    <div class=" col-md-6">
        {{ Form::text('nama_golongan', null,['class' =>'form-control', 'placeholder'=> 'ex. IV/e']) }} 
        @if ($errors->has('nama_golongan'))
        <span class="help-block">
            <strong>{{ $errors->first('nama_golongan') }}</strong>
        </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('keterangan_golongan') ? ' has-error' : '' }}">
    {{ Form::label('keterangan_golongan', null, ['class' =>'col-sm-2 control-label']) }}
    <div class=" col-md-6">
        {{ Form::text('keterangan_golongan', null,['class' =>'form-control', 'placeholder'=> 'ex. Pembina Utama']) }} 
        @if ($errors->has('keterangan_golongan'))
        <span class="help-block">
            <strong>{{ $errors->first('keterangan_golongan') }}</strong>
        </span>
        @endif
    </div>
</div>

@include(
    'template.button-form'
)