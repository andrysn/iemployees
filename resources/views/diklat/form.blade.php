{{ Form::hidden('pekerja_id', Request::segment(2)) }}

<div class="form-group{{ $errors->has('nama_diklat') ? ' has-error' : '' }}">
    {{ Form::label('nama_diklat', null, ['class' =>'col-sm-2 control-label']) }}
    <div class=" col-md-6">
        {{ Form::text('nama_diklat', null,['class' =>'form-control', 'placeholder'=> 'Nama Diklat']) }} 
        @if ($errors->has('nama_diklat'))
        <span class="help-block">
            <strong>{{ $errors->first('nama_diklat') }}</strong>
        </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('jenis_diklat') ? ' has-error' : '' }}">
    {{ Form::label('jenis_diklat', null, ['class' =>'col-sm-2 control-label']) }}
    <div class=" col-md-6">
        {{ Form::text('jenis_diklat', null,['class' =>'form-control', 'placeholder'=> 'Jenis Diklat']) }} 
        @if ($errors->has('jenis_diklat'))
        <span class="help-block">
            <strong>{{ $errors->first('jenis_diklat') }}</strong>
        </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('penyelenggara') ? ' has-error' : '' }}">
    {{ Form::label('penyelenggara', null, ['class' =>'col-sm-2 control-label']) }}
    <div class=" col-md-6">
        {{ Form::text('penyelenggara', null,['class' =>'form-control', 'placeholder'=> 'Nomor Ijazah']) }} 
        @if ($errors->has('penyelenggara'))
        <span class="help-block">
            <strong>{{ $errors->first('penyelenggara') }}</strong>
        </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('foto_diklat') ? ' has-error' : '' }}">
    {{Form::label('foto_diklat', null, ['class'=>'col-sm-2 control-label']) }}
    <div class=" col-md-6">
        {{ Form::file('foto_diklat') }}
        <span>Type JPEG, JPG, PNG</span>
        @if (isset($data))
            <p>{!! Html::image(asset('asset/foto-diklat/'.$data->foto_diklat), null, ['class'=> 'img-rounded img-responsive' , 'width' => '40%']) !!}</p>
        @endif
        @if($errors->has('foto_diklat'))
        <span class="help-block">
            <strong>{{ $errors->first('foto_diklat') }}</strong>
        </span>
        @endif 
    </div> 
</div>

<div class="form-group{{ $errors->has('tanggal_masuk') ? ' has-error' : '' }}">
    {{ Form::label('tanggal_masuk', null,['class' =>'col-sm-2 control-label'])}}
    <div class=" col-md-6">
        {{ Form::date('tanggal_masuk', null,['class' =>'form-control', 'placeholder'=> 'Tanggal Masuk']) }} 
        @if ($errors->has('tanggal_masuk'))
        <span class="help-block">
            <strong>{{ $errors->first('tanggal_masuk')}}</strong>
        </span>
        @endif 
    </div>
</div>
                
<div class="form-group">
    <div class="col-sm-8 col-sm-offset-4">
        {!! Form::submit($button, ['class' => 'btn btn-primary']) !!}
    </div>
</div>