<div class="form-group{{ $errors->has('keterangan_cuti') ? ' has-error' : '' }}">
    {{ Form::label('keterangan_cuti', null, ['class' =>'col-sm-2 control-label']) }}
    <div class=" col-md-6">
        {{ Form::text('keterangan_cuti', null,['class' =>'form-control', 'placeholder'=> 'Keterangan Cuti']) }} 
        @if ($errors->has('keterangan_cuti'))
        <span class="help-block">
            <strong>{{ $errors->first('keterangan_cuti') }}</strong>
        </span>
        @endif
    </div>
</div>

@include(
    'template.button-form'
)