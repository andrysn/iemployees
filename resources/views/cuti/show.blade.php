@extends('layouts.admin')

@section('content')
 
    <div class="row">
        <div class="col-md-12">
            <div class="box-widget widget-module">
                <div class="widget-head clearfix">
                    <span class="h-icon"><i class="fa fa-bars"></i></span>
                    <h4>Detail Data</h4>
                </div>
                
                <div class="widget-container">
                    <div class=" widget-block">
                        <label class="col-md-3 control-label">Keterangan Cuti</label>
                        <dl>
                            <dt>{{ $data->keterangan_cuti }}</dt>
                        </dl>

                        @include('template.kembali')

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection