<div class="form-group{{ $errors->has('status_pegawai') ? ' has-error' : '' }}">
    {{ Form::label('status_pegawai', null, ['class' =>'col-sm-2 control-label']) }}
    <div class=" col-md-6">
        {{ Form::text('status_pegawai', null,['class' =>'form-control', 'placeholder'=> 'Status Pegawai']) }} 
        @if ($errors->has('status_pegawai'))
        <span class="help-block">
            <strong>{{ $errors->first('status_pegawai') }}</strong>
        </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('kode_status_pegawai') ? ' has-error' : '' }}">
    {{ Form::label('keterangan_status_pegawai', null, ['class' =>'col-sm-2 control-label']) }}
    <div class=" col-md-6">
        {{ Form::text('kode_status_pegawai', null,['class' =>'form-control', 'placeholder'=> 'Keterangan Status Pegawai']) }} 
        @if ($errors->has('kode_status_pegawai'))
        <span class="help-block">
            <strong>{{ $errors->first('kode_status_pegawai') }}</strong>
        </span>
        @endif
    </div>
</div>

@include(
    'template.button-form'
)