@extends('layouts.admin')

@section('content')

<div class="row">
	<div class="col-md-12">
		<div class="box-widget widget-module">
			@include(
				'template.tambah'
			)

			<div class="widget-container">
				<div class="widget-block">
					<table id="users-table" class="table table-hover">
					<thead>
					<tr>
						<th>
							No
						</th>
						<th>
							Nama Bidang
						</th>
						<th>
							Keterangan Bidang
						</th>
						<th>
							Status
						</th>
						<th>
							Action
						</th>
					</tr>
					</thead>
					<tbody>
						@foreach ($bidang as $index => $data)
						<tr>
						<td>
							{{ $index + 1 + ( ( $bidang->CurrentPage() - 1 ) * $bidang->PerPage() ) }}
						</td>
						<td>
							{{ $data->nama_bidang }}
						</td>
						<td>
							{{ $data->kode_bidang }}
						</td>
						<td>
							{{ $data->deleted_at }}
						</td>
						<td class="tc-center">
							@include(
								'template.aksi'
							)
						</td>
					</tr>
		            @endforeach
        			</tbody>
		  			</table>
					
				</div>
			</div>
		</div>
	</div>

</div>


@endsection
