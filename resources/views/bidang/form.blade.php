<div class="form-group{{ $errors->has('nama_bidang') ? ' has-error' : '' }}">
    {{ Form::label('nama_bidang', null, ['class' =>'col-sm-2 control-label']) }}
    <div class=" col-md-6">
        {{ Form::text('nama_bidang', null,['class' =>'form-control', 'placeholder'=> 'Nama Bidang']) }} 
        @if ($errors->has('nama_bidang'))
        <span class="help-block">
            <strong>{{ $errors->first('nama_bidang') }}</strong>
        </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('kode_bidang') ? ' has-error' : '' }}">
    {{ Form::label('keterangan_bidang', null, ['class' =>'col-sm-2 control-label']) }}
    <div class=" col-md-6">
        {{ Form::text('kode_bidang', null,['class' =>'form-control', 'placeholder'=> 'Keterangan Bidang']) }} 
        @if ($errors->has('kode_bidang'))
        <span class="help-block">
            <strong>{{ $errors->first('kode_bidang') }}</strong>
        </span>
        @endif
    </div>
</div>

@include(
    'template.button-form'
)