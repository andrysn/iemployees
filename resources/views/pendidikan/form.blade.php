{{ Form::hidden('pekerja_id', Request::segment(2)) }}

<div class="form-group{{ $errors->has('jenjang') ? ' has-error' : '' }}">
    {{ Form::label('jenjang', null, ['class' =>'col-sm-2 control-label']) }}
    <div class=" col-md-6">
        {{ Form::text('jenjang', null,['class' =>'form-control', 'placeholder'=> 'Jenjang Pendidikan']) }} 
        @if ($errors->has('jenjang'))
        <span class="help-block">
            <strong>{{ $errors->first('jenjang') }}</strong>
        </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('nama_akademi') ? ' has-error' : '' }}">
    {{ Form::label('nama_akademi', null, ['class' =>'col-sm-2 control-label']) }}
    <div class=" col-md-6">
        {{ Form::text('nama_akademi', null,['class' =>'form-control', 'placeholder'=> 'Nama Akademi/Universitas']) }} 
        @if ($errors->has('nama_akademi'))
        <span class="help-block">
            <strong>{{ $errors->first('nama_akademi') }}</strong>
        </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('nomor_ijazah') ? ' has-error' : '' }}">
    {{ Form::label('nomor_ijazah', null, ['class' =>'col-sm-2 control-label']) }}
    <div class=" col-md-6">
        {{ Form::text('nomor_ijazah', null,['class' =>'form-control', 'placeholder'=> 'Nomor Ijazah']) }} 
        @if ($errors->has('nomor_ijazah'))
        <span class="help-block">
            <strong>{{ $errors->first('nomor_ijazah') }}</strong>
        </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('foto_ijazah') ? ' has-error' : '' }}">
    {{Form::label('foto_ijazah', 'Foto Ijazah', ['class'=>'col-sm-2 control-label']) }}
    <div class=" col-md-6">
        {{ Form::file('foto_ijazah') }}
        <span>Type JPEG, JPG, PNG</span>
        @if (isset($data))
            <p>{!! Html::image(asset('asset/foto-ijazah/'.$data->foto_ijazah), null, ['class'=> 'img-rounded img-responsive' , 'width' => '40%']) !!}</p>
        @endif
        @if($errors->has('foto_ijazah'))
        <span class="help-block">
            <strong>{{ $errors->first('foto_ijazah') }}</strong>
        </span>
        @endif 
    </div> 
</div>

                
<div class="form-group">
    <div class="col-sm-8 col-sm-offset-4">
        {!! Form::submit($button, ['class' => 'btn btn-primary']) !!}
    </div>
</div>