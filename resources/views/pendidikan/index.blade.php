@extends('layouts.admin')

@section('content')

<div class="row">
	<div class="col-md-12">
		<div class="box-widget widget-module">
				<div class="widget-head clearfix">
                    <span class="h-icon"><i class="fa fa-bars"></i></span>
                    <h4>{!! link_to_route('pekerja.show', 'Kembail', Request::segment(2), ['class' => 'btn btn-default'] ) !!}</h4>
                </div>

			<div class="widget-container">
				<div class=" widget-block">
            
            @include('template.notification')

            {!! Form::open(
               ['route' => [$route.'.store'], 
                'role'  => 'form',
                'method'=> 'post',
                'class' => 'form-horizontal',
                'files' => 'true']) !!}

                @include(
                    $route.'.form'
                )


            {!! Form::close() !!}

				</div>
			</div>

			<div class="widget-container">
				<div class="widget-block">
					<table id="users-table" class="table table-hover">
					<thead>
					<tr>
						<th>
							No
						</th>
						<th>
							Jenjang Pendidikan
						</th>
						<th>
							Nama Akademi/Universitas
						</th>
						<th>
							Nomor Ijazah
						</th>
						<th>
							File Ijazah
						</th>
						<th>
							Aksi
						</th>
					</tr>
					</thead>
					<tbody>
					@foreach ($pendidikan as $index => $data)
						<tr>
						<td>
							{{ $index + 1 + ( ( $pendidikan->CurrentPage() - 1 ) * $pendidikan->PerPage() ) }}
						</td>
						<td>
							{{ $data->jenjang }}
						</td>
						<td>
							{{ $data->nama_akademi }}
						</td>
						<td>
							{{ $data->nomor_ijazah }}
						</td>
						<td>
							{{ Html::image('asset/foto-ijazah/' . $data->foto_ijazah, 'alt', ['width' => '30%']) }}
						</td>
						<td class="tc-center">
<div class="btn-toolbar" role="toolbar">

        <div class="btn-employees" role="employees">
            {!! Form::open(['route' => [$route.'.destroy', $data->id], 'method' => 'delete', 'class' => 'form-inline', 'id' => "delete-form"]) !!}
                {!! Form::submit('Hapus', ['class' => 'btn btn-danger btn-sm', 'onclick' => "return confirmation();"]) !!}

            {!! Form::close() !!}
        </div>

</div>
						</td>
					</tr>
		            @endforeach
        			</tbody>
		  			</table>
					
				</div>
			</div>
		</div>
	</div>

</div>


@endsection
