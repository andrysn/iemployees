@extends('layouts.admin')

@section('content')
 
    <div class="row">
        <div class="col-md-12">
            <div class="box-widget widget-module">
                <div class="widget-head clearfix">
                    <span class="h-icon"><i class="fa fa-bars"></i></span>
                    <h4>Detail Data</h4>
                </div>
                
                <div class="widget-container">
                    <div class=" widget-block">
                    @foreach ($modul as $index => $data)
                    
                    
                        <label class="col-md-3 control-label">Keterangan Akses</label>
                        <dl>
                            <dt>{{ $data->modul->nama_modul }}</dt>
                        </dl>

                    @endforeach
                        @include('template.kembali')
                    </div>
                </div>
            </div>
        </div>
    </div>


@include(
    'template.kembali'
)

@endsection