<div class="form-group{{ $errors->has('nama_jabatan') ? ' has-error' : '' }}">
    {{ Form::label('nama_jabatan', null, ['class' =>'col-sm-2 control-label']) }}
    <div class=" col-md-6">
        {{ Form::text('nama_jabatan', null,['class' =>'form-control', 'placeholder'=> 'Nama Jabatan']) }} 
        @if ($errors->has('nama_jabatan'))
        <span class="help-block">
            <strong>{{ $errors->first('nama_jabatan') }}</strong>
        </span>
        @endif
    </div>
</div>

@include(
    'template.button-form'
)