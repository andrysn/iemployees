@extends('layouts.admin')

@section('content')

<div class="row">
	<div class="col-md-12">
		<div class="box-widget widget-module">
				<div class="widget-head clearfix">
                    <span class="h-icon"><i class="fa fa-bars"></i></span>
                    <h4>{!! link_to_route('pekerja.show', 'Kembail', Request::segment(2), ['class' => 'btn btn-default'] ) !!}</h4>
                </div>

			<div class="widget-container">
				<div class=" widget-block">
            
            @include('template.notification')

            {!! Form::open(
               ['route' => [$route.'.store'], 
                'role'  => 'form',
                'method'=> 'post',
                'class' => 'form-horizontal',
                'files' => 'true']) !!}

                @include(
                    $route.'.form'
                )


            {!! Form::close() !!}

				</div>
			</div>

			<div class="widget-container">
				<div class="widget-block">
					<table id="users-table" class="table table-hover">
					<thead>
					<tr>
						<th>
							No
						</th>
						<th>
							Nama Organisasi
						</th>
						<th>
							Jabatan
						</th>
						<th>
							Keterangan
						</th>
						<th>
							Masa Aktif
						</th>
						<th>
							Aksi
						</th>
					</tr>
					</thead>
					<tbody>
					@foreach ($organisasi as $index => $data)
						<tr>
						<td>
							{{ $index + 1 + ( ( $organisasi->CurrentPage() - 1 ) * $organisasi->PerPage() ) }}
						</td>
						<td>
							{{ $data->nama_organisasi }}
						</td>
						<td>
							{{ $data->jabatan }}
						</td>
						<td>
							{{ $data->keterangan }}
						</td>
						<td>
							{{ $data->tanggal_masuk }} s/d {{ $data->tanggal_keluar }}
						</td>
						<td class="tc-center">
<div class="btn-toolbar" role="toolbar">

        <div class="btn-employees" role="employees">
            {!! Form::open(['route' => [$route.'.destroy', $data->id], 'method' => 'delete', 'class' => 'form-inline', 'id' => "delete-form"]) !!}
                {!! Form::submit('Hapus', ['class' => 'btn btn-danger btn-sm', 'onclick' => "return confirmation();"]) !!}

            {!! Form::close() !!}
        </div>

</div>
						</td>
					</tr>
		            @endforeach
        			</tbody>
		  			</table>
					
				</div>
			</div>
		</div>
	</div>

</div>


@endsection
