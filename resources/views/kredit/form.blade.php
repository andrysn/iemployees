{{ Form::hidden('pekerja_id', Request::segment(2)) }}

<div class="form-group{{ $errors->has('penetapan') ? ' has-error' : '' }}">
    {{ Form::label('penetapan', null, ['class' =>'col-sm-2 control-label']) }}
    <div class=" col-md-6">
        {{ Form::text('penetapan', null,['class' =>'form-control', 'placeholder'=> 'Penetapan']) }} 
        @if ($errors->has('penetapan'))
        <span class="help-block">
            <strong>{{ $errors->first('penetapan') }}</strong>
        </span>
        @endif
    </div>
</div>


<div class="form-group{{ $errors->has('angka_kredit_lama') ? ' has-error' : '' }}">
    {{ Form::label('angka_kredit_lama', null, ['class' =>'col-sm-2 control-label']) }}
    <div class=" col-md-6">
        {{ Form::text('angka_kredit_lama', null,['class' =>'form-control', 'placeholder'=> 'Angka Kredit Lama']) }} 
        @if ($errors->has('angka_kredit_lama'))
        <span class="help-block">
            <strong>{{ $errors->first('angka_kredit_lama') }}</strong>
        </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('angka_kredit_baru') ? ' has-error' : '' }}">
    {{ Form::label('angka_kredit_baru', null, ['class' =>'col-sm-2 control-label']) }}
    <div class=" col-md-6">
        {{ Form::text('angka_kredit_baru', null,['class' =>'form-control', 'placeholder'=> 'Angka Kredit Baru']) }} 
        @if ($errors->has('angka_kredit_baru'))
        <span class="help-block">
            <strong>{{ $errors->first('angka_kredit_baru') }}</strong>
        </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('jumlah') ? ' has-error' : '' }}">
    {{ Form::label('jumlah', null, ['class' =>'col-sm-2 control-label']) }}
    <div class=" col-md-6">
        {{ Form::text('jumlah', null,['class' =>'form-control', 'placeholder'=> 'Jumlah']) }} 
        @if ($errors->has('jumlah'))
        <span class="help-block">
            <strong>{{ $errors->first('jumlah') }}</strong>
        </span>
        @endif
    </div>
</div>

                
<div class="form-group">
    <div class="col-sm-8 col-sm-offset-4">
        {!! Form::submit($button, ['class' => 'btn btn-primary']) !!}
    </div>
</div>