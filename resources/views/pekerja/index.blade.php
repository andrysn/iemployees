@extends('layouts.admin')

@section('content')

<div class="row">
	<div class="col-md-12">
		<div class="box-widget widget-module">
			@include(
				'template.tambah'
			)

			<div class="widget-container">
				<div class="widget-block">
					<table id="users-table" class="table table-hover" width="150px">
					<thead>
					<tr>
						<th>
							No
						</th>
						<th width="150px">
							Foto
						</th>
						<th>
							Nama Pegawai
						</th>
						<th>
							Jabatan / Tingkat/Golongan
						</th>
						<th>
							Status Pegawai
						</th>
						<th>
							Aksi
						</th>
					</tr>
					</thead>
					
					<tbody>
					@foreach ($pekerja as $index =>$data)
					<tr>
						<td>
							{{ $index + 1 + ( ($pekerja->CurrentPage() - 1) * $pekerja->PerPage() ) }}
						</td>
						<td>
							{{ Html::image('asset/foto-pegawai/' . $data->foto, 'alt', ['width' => '100%']) }}
						</td>
						<td>
							{{ $data->nama_pegawai }}
						</td>
						<td>
							{{ $data->jabatan->nama_jabatan }} {{ $data->bidang->nama_bidang }} / {{ $data->pangkatGolongan->nama_golongan }} / {{ $data->pangkatEselon->nama_eselon }}
						</td>
						<td>
							{{ $data->status }}
						</td>
						<td class="tc-center">
							@include('template.aksi')
						</td>
					</tr>
            @endforeach
					</tbody>
					</table>
					
				</div>
			</div>
		</div>
	</div>

</div>


@endsection
