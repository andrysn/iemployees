<ul class="nav nav-tabs">
	<li class="active"><a href="#data" data-toggle="tab">Identitas Pegawai</a></li>
	<li><a href="#data_pribadi" data-toggle="tab">Data Pribadi</a></li>
	<li><a href="#data_tambahan" data-toggle="tab">Data Tambahan</a></li>
</ul>
<!-- DIV Tab -->

<div class="tab-content">

	<div class="tab-pane active" id="data">
  <p>&nbsp;</p>

		<div class="form-group{{ $errors->has('foto') ? ' has-error' : '' }}">
			{{Form::label('foto','Foto', ['class'=>'col-sm-2 control-label']) }}
			<div class=" col-md-6">
				{{ Form::file('foto') }}
				<span>Type icon JPEG, JPG, BMP, PNG</span>
				@if (isset($data) && $data->icon)
					<p>{!! Html::image(asset('asset/foto-pegawai/'.$data->foto), null, ['class'=> 'img-rounded img-responsive' , 'width' => '40%']) !!}</p>
				@endif
				@if($errors->has('foto'))
				<span class="help-block">
					<strong>{{ $errors->first('foto') }}</strong>
				</span>
				@endif 
			</div> 
		</div>

		<div class="form-group{{ $errors->has('nip') ? ' has-error' : '' }}">
			{{ Form::label('nip', null,['class' =>'col-sm-2 control-label'])}}
			<div class=" col-md-6">
				{{ Form::text('nip', null,['class' =>'form-control', 'placeholder'=> 'NIP'])}}
				@if ($errors->has('nip'))
				<span class="help-block">
					<strong>{{ $errors->first('nip')}}</strong>
				</span>
				@endif 
			</div>
		</div>

		<div class="form-group{{ $errors->has('jabatan_id') ? ' has-error' : '' }}">
			{{ Form::label('jabatan', null,['class' =>'col-sm-2 control-label']) }}
			<div class=" col-md-6">
				{{ Form::select('jabatan_id', $jabatan, null, ['class' =>'form-control selectpicker', 'placeholder'=>'Pilih Jabatan...', 'data-live-search'=>'true']) }}
				@if ($errors->has('jabatan_id'))
				<span class="help-block">
					<strong>{{ $errors->first('jabatan_id')}}</strong>
				</span>
				@endif 
			</div>
		</div>

		<div class="form-group{{ $errors->has('bidang_id') ? ' has-error' : '' }}">
			{{ Form::label('bidang', null,['class' =>'col-sm-2 control-label'])}}
			<div class=" col-md-6">
				{{ Form::select('bidang_id', $bidang, null, ['class' =>'form-control selectpicker', 'placeholder'=>'Pilih Bidang...', 'data-live-search'=>'true'])}}
				@if ($errors->has('bidang_id'))
				<span class="help-block">
					<strong>{{ $errors->first('bidang_id')}}</strong>
				</span>
				@endif 
			</div>
		</div>

		<div class="form-group{{ $errors->has('status_pegawai_id') ? ' has-error' : '' }}">
			{{ Form::label('status_pegawai', null,['class' =>'col-sm-2 control-label'])}}
			<div class=" col-md-6">
				{{ Form::select('status_pegawai_id', $statusPegawai, null, ['class' =>'form-control selectpicker', 'placeholder'=>'Pilih Status Pegawai...', 'data-live-search'=>'true'])}}
				@if ($errors->has('status_pegawai_id'))
				<span class="help-block">
					<strong>{{ $errors->first('status_pegawai_id')}}</strong>
				</span>
				@endif 
			</div>
		</div>

		<div class="form-group{{ $errors->has('pangkat_golongan_id') ? ' has-error' : '' }}">
			{{ Form::label('pangkat_golongan', null,['class' =>'col-sm-2 control-label'])}}
			<div class=" col-md-6">
				{{ Form::select('pangkat_golongan_id', $pangkatGolongan, null, ['class' =>'form-control selectpicker', 'placeholder'=>'Pilih Pangkat Golongan...', 'data-live-search'=>'true'])}}
				@if ($errors->has('pangkat_golongan_id'))
				<span class="help-block">
					<strong>{{ $errors->first('pangkat_golongan_id')}}</strong>
				</span>
				@endif 
			</div>
		</div>

		<div class="form-group{{ $errors->has('pangkat_eselon_id') ? ' has-error' : '' }}">
			{{ Form::label('pangkat_eselon', null,['class' =>'col-sm-2 control-label'])}}
			<div class=" col-md-6">
				{{ Form::select('pangkat_eselon_id', $pangkatEselon, null, ['class' =>'form-control selectpicker', 'placeholder'=>'Pilih Pangkat Eselon...', 'data-live-search'=>'true'])}}
				@if ($errors->has('pangkat_eselon_id'))
				<span class="help-block">
					<strong>{{ $errors->first('pangkat_eselon_id')}}</strong>
				</span>
				@endif 
			</div>
		</div>

    <div class="form-group{{ $errors->has('keterangan_pegawai') ? ' has-error' : '' }}">
      {{ Form::label('keterangan_pegawai', null,['class' =>'col-sm-2 control-label'])}}
      <div class=" col-md-6">
        {{ Form::text('keterangan_pegawai', null,['class' =>'form-control', 'placeholder'=> 'Keterangan Pegawai'])}} 
        @if ($errors->has('keterangan_pegawai'))
        <span class="help-block">
          <strong>{{ $errors->first('keterangan_pegawai')}}</strong>
        </span>
        @endif 
      </div>
    </div>

  </div>

  <div class="tab-pane" id="data_pribadi">
  <p>&nbsp;</p>
    <div class="form-group{{ $errors->has('nama_pegawai') ? ' has-error' : '' }}">
      {{ Form::label('nama_pegawai', null,['class' =>'col-sm-2 control-label'])}}
      <div class=" col-md-6">
        {{ Form::text('nama_pegawai', null,['class' =>'form-control', 'placeholder'=> 'Nama Pegawai'])}} 
        @if ($errors->has('nama_pegawai'))
        <span class="help-block">
          <strong>{{ $errors->first('nama_pegawai')}}</strong>
        </span>
        @endif 
      </div>
    </div>

    <div class="form-group{{ $errors->has('alamat') ? ' has-error' : '' }}">
      {{ Form::label('alamat', null,['class' =>'col-sm-2 control-label'])}}
      <div class=" col-md-6">
        {{ Form::text('alamat', null,['class' =>'form-control', 'placeholder'=> 'Alamat'])}} 
        @if ($errors->has('alamat'))
        <span class="help-block">
          <strong>{{ $errors->first('alamat')}}</strong>
        </span>
        @endif 
      </div>
    </div>

    <div class="form-group{{ $errors->has('tempat_lahir') ? ' has-error' : '' }}">
      {{ Form::label('tempat_lahir', null,['class' =>'col-sm-2 control-label'])}}
      <div class=" col-md-6">
        {{ Form::text('tempat_lahir', null,['class' =>'form-control', 'placeholder'=> 'Tempat Lahir'])}} 
        @if ($errors->has('tempat_lahir'))
        <span class="help-block">
          <strong>{{ $errors->first('tempat_lahir')}}</strong>
        </span>
        @endif 
      </div>
    </div>

    <div class="form-group{{ $errors->has('tanggal_lahir') ? ' has-error' : '' }}">
      {{ Form::label('tanggal_lahir', null,['class' =>'col-sm-2 control-label']) }}
      <div class=" col-md-6">
        {{ Form::date('tanggal_lahir', null,['class' =>'form-control', 'placeholder'=> 'Tanggal Lahir'])}} 
        @if ($errors->has('tanggal_lahir'))
        <span class="help-block">
          <strong>{{ $errors->first('tanggal_lahir')}}</strong>
        </span>
        @endif 
      </div>
    </div>

    <div class="form-group{{ $errors->has('jenis_kelamin') ? ' has-error' : '' }}">
      {{ Form::label('jenis_kelamin', null,['class' =>'col-sm-2 control-label'])}}
      <div class=" col-md-6">
        <select class="form-control" name="jenis_kelamin">
          <option value="1" >Laki-Laki</option>
          <option value="2">Perempuan</option>
        </select>
        @if ($errors->has('jenis_kelamin'))
        <span class="help-block">
          <strong>{{ $errors->first('jenis_kelamin')}}</strong>
        </span>
        @endif 
      </div>
    </div>

    <div class="form-group{{ $errors->has('agama') ? ' has-error' : '' }}">
      {{ Form::label('agama', null,['class' =>'col-sm-2 control-label'])}}
      <div class=" col-md-6">
        <select class="form-control" name="agama">
          <option value="1" >Islam</option>
          <option value="2">Kristen</option>
          <option value="3" >Katolik</option>
          <option value="4">Hindu</option>
          <option value="5" >Budha</option>
          <option value="6">Konghucu</option>
        </select>
        @if ($errors->has('agama'))
        <span class="help-block">
          <strong>{{ $errors->first('agama')}}</strong>
        </span>
        @endif 
      </div>
    </div>

  </div>

  <div class="tab-pane" id="data_tambahan">
  <p>&nbsp;</p>

    <div class="form-group{{ $errors->has('nomor_karpeg') ? ' has-error' : '' }}">
      {{ Form::label('nomor_karpeg', null,['class' =>'col-sm-2 control-label'])}}
      <div class=" col-md-6">
        {{ Form::text('nomor_karpeg', null,['class' =>'form-control', 'placeholder'=> 'Nomor KARPEG'])}} 
        @if ($errors->has('nomor_karpeg'))
        <span class="help-block">
          <strong>{{ $errors->first('nomor_karpeg')}}</strong>
        </span>
        @endif 
      </div>
    </div>

    <div class="form-group{{ $errors->has('	nomor_bpjs') ? ' has-error' : '' }}">
      {{ Form::label('nomor_bpjs', null,['class' =>'col-sm-2 control-label'])}}
      <div class=" col-md-6">
        {{ Form::text('nomor_bpjs', null,['class' =>'form-control', 'placeholder'=> 'Nomor BPJS'])}} 
        @if ($errors->has('nomor_bpjs'))
        <span class="help-block">
          <strong>{{ $errors->first('nomor_bpjs')}}</strong>
        </span>
        @endif 
      </div>
    </div>

    <div class="form-group{{ $errors->has('nomor_pokok_wajib_pajak') ? ' has-error' : '' }}">
      {{ Form::label('nomor_pokok_wajib_pajak', null,['class' =>'col-sm-2 control-label'])}}
      <div class=" col-md-6">
        {{ Form::text('nomor_pokok_wajib_pajak', null,['class' =>'form-control', 'placeholder'=> 'Nomor Pokok Wajib Pajak'])}} 
        @if ($errors->has('nomor_pokok_wajib_pajak'))
        <span class="help-block">
          <strong>{{ $errors->first('nomor_pokok_wajib_pajak')}}</strong>
        </span>
        @endif 
      </div>
    </div>

  @include(
      'template.button-form'
  )
  </div>
</div>