@extends('layouts.admin')

@section('content')
 
     <div class="row">
        <div class="col-md-12">
            <div class="box-widget widget-module">
                <div class="widget-head clearfix">
                    <span class="h-icon"><i class="fa fa-bars"></i></span>
                    <h4>Detail Data</h4>
                </div>

                    <div class="FontAwsome matmix-icons-list">
                        <a href="{{ route('pendidikan.show', $data->id) }}" title="pendidikan"><i class="fa fa-graduation-cap"></i></a>
                        <a href="{{ route('diklat.show', $data->id) }}" title="diklat"><i class="fa fa-book"></i></a>
                        <a href="{{ route('riwayat-kerja.show', $data->id) }}" title="pengalaman kerja"><i class="fa fa-briefcase"></i></a>
                        <a href="{{ route('keluarga.show', $data->id) }}" title="keluarga"><i class="fa fa-users"></i></a>
                        <a href="{{ route('kredit.show', $data->id) }}" title="angka kredit"><i class="fa fa-bar-chart"></i></a>
                        <a href="{{ route('organisasi.show', $data->id) }}" title="organisasi"><i class="fa fa-bullhorn"></i></a>
                        <a href="{{ route('penghargaan.show', $data->id) }}" title="penghargaan"><i class="fa fa-trophy"></i></a>
                    </div>
                    
            <div class="widget-container">
                <div class=" widget-block">

                <label class="col-md-3 control-label">Foto</label>
                <dl>
                    <dt>{{ Html::image('asset/foto-pegawai/' . $data->foto, 'alt', ['width' => '30%']) }}</dt>
                </dl>

                <label class="col-md-3 control-label">NIP</label>
                <dl>
                    <dt>{{ $data->nip}}</dt>
                </dl>

                <label class="col-md-3 control-label">Jabatan</label>
                <dl>
                    <dt>{{ $data->jabatan->nama_jabatan}} {{ $data->bidang->nama_bidang }}</dt>
                </dl>

                <label class="col-md-3 control-label">Pangkat/Golongan</label>
                <dl>
                    <dt>{{ $data->pangkatGolongan->nama_golongan }} / {{ $data->pangkatEselon->nama_eselon }}</dt>
                </dl>

                <label class="col-md-3 control-label">Status Kepegawaian</label>
                <dl>
                    <dt>{{ $data->statusPegawai->status_pegawai }}</dt>
                </dl>

                <label class="col-md-3 control-label">Nama Pegawai</label>
                <dl>
                    <dt>{{ $data->nama_pegawai }}</dt>
                </dl>

                <label class="col-md-3 control-label">Alamat</label>
                <dl>
                    <dt>{{ $data->alamat }}</dt>
                </dl>

                <label class="col-md-3 control-label">Tempat, Tanggal Lahir</label>
                <dl>
                    <dt>{{ $data->tempat_lahir }}, {{ $data->tanggal_lahir }}</dt>
                </dl>

                <label class="col-md-3 control-label">Jenis Kelamin</label>
                <dl>
                    <dt>{{ $data->jenis_kelamin }}</dt>
                </dl>

                <label class="col-md-3 control-label">Agama</label>
                <dl>
                    <dt>{{ $data->faith }}</dt>
                </dl>

                <label class="col-md-3 control-label">Keterangan Pegawai</label>
                <dl>
                    <dt>{{ $data->keterangan_pegawai }}</dt>
                </dl>

                <label class="col-md-3 control-label">Nomor KARPEG</label>
                <dl>
                    <dt>{{ $data->nomor_karpeg }}</dt>
                </dl>

                <label class="col-md-3 control-label">Nomor BPJS</label>
                <dl>
                    <dt>{{ $data->nomor_bpjs }}</dt>
                </dl>

                <label class="col-md-3 control-label">Nomor Pokok Wajib Pajak</label>
                <dl>
                    <dt>{{ $data->nomor_pokok_wajib_pajak }}</dt>
                </dl>
    
                <dl>
                    <a class="btn btn-default" href="{{ route($route.'.index') }}">Kembali</a>
                </dl>

                </div>
            </div>
        </div>
    </div>
</div>

</div>
@endsection