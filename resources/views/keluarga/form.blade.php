{{ Form::hidden('pekerja_id', Request::segment(2)) }}

<div class="form-group{{ $errors->has('nik') ? ' has-error' : '' }}">
    {{ Form::label('nik', null, ['class' =>'col-sm-2 control-label']) }}
    <div class=" col-md-6">
        {{ Form::text('nik', null,['class' =>'form-control', 'placeholder'=> 'NIK']) }} 
        @if ($errors->has('nik'))
        <span class="help-block">
            <strong>{{ $errors->first('nik') }}</strong>
        </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('nama_lengkap') ? ' has-error' : '' }}">
    {{ Form::label('nama_lengkap', null, ['class' =>'col-sm-2 control-label']) }}
    <div class=" col-md-6">
        {{ Form::text('nama_lengkap', null,['class' =>'form-control', 'placeholder'=> 'Nama Lengkap']) }} 
        @if ($errors->has('nama_lengkap'))
        <span class="help-block">
            <strong>{{ $errors->first('nama_lengkap') }}</strong>
        </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('jenis_kelamin') ? ' has-error' : '' }}">
    {{ Form::label('jenis_kelamin', null,['class' =>'col-sm-2 control-label'])}}
    <div class=" col-md-6">
        <select class="form-control" name="jenis_kelamin">
            <option value="1" >Laki-Laki</option>
            <option value="2">Perempuan</option>
        </select>
        @if ($errors->has('jenis_kelamin'))
        <span class="help-block">
            <strong>{{ $errors->first('jenis_kelamin')}}</strong>
        </span>
        @endif 
    </div>
</div>

<div class="form-group{{ $errors->has('hubungan_keluarga') ? ' has-error' : '' }}">
    {{ Form::label('hubungan_keluarga', null, ['class' =>'col-sm-2 control-label']) }}
    <div class=" col-md-6">
        {{ Form::text('hubungan_keluarga', null,['class' =>'form-control', 'placeholder'=> 'Hubungan Keluarga']) }} 
        @if ($errors->has('hubungan_keluarga'))
        <span class="help-block">
            <strong>{{ $errors->first('hubungan_keluarga') }}</strong>
        </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('pekerjaan') ? ' has-error' : '' }}">
    {{ Form::label('pekerjaan', null, ['class' =>'col-sm-2 control-label']) }}
    <div class=" col-md-6">
        {{ Form::text('pekerjaan', null,['class' =>'form-control', 'placeholder'=> 'Pekerjaan']) }} 
        @if ($errors->has('pekerjaan'))
        <span class="help-block">
            <strong>{{ $errors->first('pekerjaan') }}</strong>
        </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('tempat_lahir') ? ' has-error' : '' }}">
    {{ Form::label('tempat_lahir', null, ['class' =>'col-sm-2 control-label']) }}
    <div class=" col-md-6">
        {{ Form::text('tempat_lahir', null,['class' =>'form-control', 'placeholder'=> 'Tempat Lahir']) }} 
        @if ($errors->has('tempat_lahir'))
        <span class="help-block">
            <strong>{{ $errors->first('tempat_lahir') }}</strong>
        </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('tanggal_lahir') ? ' has-error' : '' }}">
    {{ Form::label('tanggal_lahir', null,['class' =>'col-sm-2 control-label'])}}
    <div class=" col-md-6">
        {{ Form::date('tanggal_lahir', null,['class' =>'form-control', 'placeholder'=> 'Tanggal Lahir']) }} 
        @if ($errors->has('tanggal_lahir'))
        <span class="help-block">
            <strong>{{ $errors->first('tanggal_lahir')}}</strong>
        </span>
        @endif 
    </div>
</div>

<div class="form-group{{ $errors->has('agama') ? ' has-error' : '' }}">
    {{ Form::label('agama', null,['class' =>'col-sm-2 control-label'])}}
    <div class=" col-md-6">
    <select class="form-control" name="agama">
        <option value="1" >Islam</option>
        <option value="2">Kristen</option>
        <option value="3" >Katolik</option>
        <option value="4">Hindu</option>
        <option value="5" >Budha</option>
        <option value="6">Konghucu</option>
    </select>
    @if ($errors->has('agama'))
    <span class="help-block">
        <strong>{{ $errors->first('agama')}}</strong>
    </span>
    @endif 
    </div>
</div>
                
<div class="form-group">
    <div class="col-sm-8 col-sm-offset-4">
        {!! Form::submit($button, ['class' => 'btn btn-primary']) !!}
    </div>
</div>