@extends('layouts.admin')

@section('content')

<div class="row">
	<div class="col-md-12">
		<div class="box-widget widget-module">
				<div class="widget-head clearfix">
                    <span class="h-icon"><i class="fa fa-bars"></i></span>
                    <h4>{!! link_to_route('pekerja.show', 'Kembail', Request::segment(2), ['class' => 'btn btn-default'] ) !!}</h4>
                </div>

			<div class="widget-container">
				<div class=" widget-block">
            
            @include('template.notification')

            {!! Form::open(
               ['route' => [$route.'.store'], 
                'role'  => 'form',
                'method'=> 'post',
                'class' => 'form-horizontal',
                'files' => 'true']) !!}

                @include(
                    $route.'.form'
                )


            {!! Form::close() !!}

				</div>
			</div>

			<div class="widget-container">
				<div class="widget-block">
					<table id="users-table" class="table table-hover">
					<thead>
					<tr>
						<th>
							No
						</th>
						<th>
							Hubungan Keluarga
						</th>
						<th>
							Nama Lengkap (L/P)
						</th>
						<th>
							NIK
						</th>
						<th>
							Tempat Tanggal Lahir
						</th>
						<th>
							Pekerjaan
						</th>
						<th>
							Aksi
						</th>
					</tr>
					</thead>
					<tbody>
					@foreach ($keluarga as $index => $data)
						<tr>
						<td>
							{{ $index + 1 + ( ( $keluarga->CurrentPage() - 1 ) * $keluarga->PerPage() ) }}
						</td>
						<td>
							{{ $data->hubungan_keluarga }}
						</td>
						<td>
							{{ $data->nama_lengkap }} ({{ $data->jenis_kelamin }})
						</td>
						<td>
							{{ $data->nik }}
						</td>
						<td>
							{{ $data->tempat_lahir }} {{ $data->tanggal_lahir}}
						</td>
						<td>
							{{ $data->pekerjaan }}
						</td>
						<td class="tc-center">
<div class="btn-toolbar" role="toolbar">

        <div class="btn-employees" role="employees">
            {!! Form::open(['route' => [$route.'.destroy', $data->id], 'method' => 'delete', 'class' => 'form-inline', 'id' => "delete-form"]) !!}
                {!! Form::submit('Hapus', ['class' => 'btn btn-danger btn-sm', 'onclick' => "return confirmation();"]) !!}

            {!! Form::close() !!}
        </div>

</div>
						</td>
					</tr>
		            @endforeach
        			</tbody>
		  			</table>
					
				</div>
			</div>
		</div>
	</div>

</div>


@endsection
