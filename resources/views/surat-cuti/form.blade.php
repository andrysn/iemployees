<div class="form-group{{ $errors->has('pekerja_id') ? ' has-error' : '' }}">
    {{ Form::label('nama_pekerja', null, ['class' => 'col-sm-2 control-label']) }}
    <div class="col-md-6">
        {{ Form::select('pekerja_id', $pekerja, null, ['class' => 'form-control selectpicker', 'placeholder' => 'Pilih pekerja...', 'data-live-search' => 'true']) }}
        @if($errors->has('pekerja_id'))
            <span class="help-block">
                <strong>{{ $errors->first('pekerja_id') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('cuti_id') ? ' has-error' : '' }}">
    {{ Form::label('keterangan_cuti', null, ['class' => 'col-sm-2 control-label']) }}
    <div class="col-md-6">
        {{ Form::select('cuti_id', $cuti, null, ['class' => 'form-control selectpicker', 'placeholder' => 'Pilih keterangan cuti...', 'data-live-search' => 'true']) }}
        @if($errors->has('cuti_id'))
            <span class="help-block">
                <strong>{{ $errors->first('cuti_id') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group{{ $errors->has('tanggal_cuti') ? ' has-error' : '' }}">
    {{ Form::label('tanggal_cuti', null,['class' =>'col-sm-2 control-label'])}}
    <div class=" col-md-6">
        {{ Form::date('tanggal_cuti', null,['class' =>'form-control', 'placeholder'=> 'Tanggal cuti']) }} 
        @if ($errors->has('tanggal_cuti'))
        <span class="help-block">
            <strong>{{ $errors->first('tanggal_cuti')}}</strong>
        </span>
        @endif 
    </div>
</div>

<div class="form-group{{ $errors->has('tanggal_kembali') ? ' has-error' : '' }}">
    {{ Form::label('tanggal_kembali', null,['class' =>'col-sm-2 control-label'])}}
    <div class=" col-md-6">
        {{ Form::date('tanggal_kembali', null,['class' =>'form-control', 'placeholder'=> 'Tanggal Kembali']) }} 
        @if ($errors->has('tanggal_kembali'))
        <span class="help-block">
            <strong>{{ $errors->first('tanggal_kembali')}}</strong>
        </span>
        @endif 
    </div>
</div>
                
<div class="form-group">
    <div class="col-sm-8 col-sm-offset-4">
        {!! Form::submit($button, ['class' => 'btn btn-primary']) !!}
    </div>
</div>