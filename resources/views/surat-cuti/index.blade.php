@extends('layouts.admin')

@section('content')

<div class="row">
	<div class="col-md-12">
		<div class="box-widget widget-module">
			@include(
				'template.tambah'
			)

			<div class="widget-container">
				<div class="widget-block">
					<table id="users-table" class="table table-hover">
					<thead>
					<tr>
						<th>
							No
						</th>
						<th>
							Nama Pegawai
						</th>
						<th>
							Tanggal Cuti
						</th>
						<th>
							Keterangan Cuti
						</th>
						<th>
							Aksi
						</th>
					</tr>
					</thead>
					<tbody>
					@foreach ($suratCuti as $index => $data)
						<tr>
						<td>
							{{ $index + 1 + ( ( $suratCuti->CurrentPage() - 1 ) * $suratCuti->PerPage() ) }}
						</td>
						<td>
							{{ $data->pekerja->nip }} {{ $data->pekerja->nama_pegawai }}
						</td>
						<td>
							{{ $data->tanggal_cuti }} s/d {{ $data->tanggal_kembali }}
						</td>
						<td>
							{{ $data->cuti->keterangan_cuti }}
						</td>
						<td class="tc-center">
						@include(
							'template.aksi'
						)
						</td>
					</tr>
		            @endforeach
        			</tbody>
		  			</table>
					
				</div>
			</div>
		</div>
	</div>

</div>


@endsection
