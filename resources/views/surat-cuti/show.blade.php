@extends('layouts.admin')

@section('content')
 
     <div class="row">
        <div class="col-md-12">
            <div class="box-widget widget-module">
                <div class="widget-head clearfix">
                    <span class="h-icon"><i class="fa fa-bars"></i></span>
                    <h4>Detail Data</h4>
                </div>

            <div class="widget-container">
                <div class=" widget-block">

                <label class="col-md-3 control-label">Foto</label>
                <dl>
                    <dt>{{ Html::image('asset/foto-pegawai/' . $data->pekerja->foto, 'alt', ['width' => '30%']) }}</dt>
                </dl>

                <label class="col-md-3 control-label">NIP</label>
                <dl>
                    <dt>{{ $data->pekerja->nip}}</dt>
                </dl>

                <label class="col-md-3 control-label">Jabatan</label>
                <dl>
                    <dt>{{ $data->pekerja->jabatan->nama_jabatan}} {{ $data->bidang->nama_bidang }}</dt>
                </dl>

                <label class="col-md-3 control-label">Pangkat/Golongan</label>
                <dl>
                    <dt>{{ $data->pekerja->pangkatGolongan->nama_golongan }} / {{ $data->pangkatEselon->nama_eselon }}</dt>
                </dl>

                <label class="col-md-3 control-label">Status Kepegawaian</label>
                <dl>
                    <dt>{{ $data->pekerja->statusPegawai->status_pegawai }}</dt>
                </dl>

                <label class="col-md-3 control-label">Nama Pegawai</label>
                <dl>
                    <dt>{{ $data->pekerja->nama_pegawai }}</dt>
                </dl>

                <label class="col-md-3 control-label">Keterangan Pegawai</label>
                <dl>
                    <dt>{{ $data->pekerja->keterangan_pegawai }}</dt>
                </dl>

                <label class="col-md-3 control-label">Keterangan Cuti</label>
                <dl>
                    <dt>{{ $data->cuti->keterangan_cuti }}</dt>
                </dl>

                <label class="col-md-3 control-label">Masa Cuti</label>
                <dl>
                    <dt>{{ $data->tanggal_cuti }} s/d {{ $data->tanggal_kembali }}</dt>
                </dl>
    
                <dl>
                    <a class="btn btn-default" href="{{ route($route.'.index') }}">Kembali</a>
                </dl>

                </div>
            </div>
        </div>
    </div>
</div>

</div>
@endsection