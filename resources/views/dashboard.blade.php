@extends('layouts.admin')

@section('content')

<div class="row">
    <div class="col-md-3 col-sm-6">
        <div class="iconic-w-wrap number-rotate">
            <span class="stat-w-title">Pegawai</span>
            <a href="{{ route('pekerja.index') }}" class="ico-cirlce-widget w_bg_cyan">
                <span><i class="fa fa-group"></i></span>
            </a>
            <div class="w-meta-info">
                <span class="w-meta-value number-animate" data-value="{{ $aktifPegawai + $nonAktifPegawai }}" data-animation-duration="1500">{{ $aktifPegawai + $nonAktifPegawai }}</span>
                <span class="w-meta-title">Orang</span>

            </div>
        </div>
    </div>

    <div class="col-md-3 col-sm-6">
        <div class="iconic-w-wrap iconic-w-wrap">
            <span class="stat-w-title">Pegawai Aktif</span>
            <a href="{{ route('pekerja.index') }}" class="ico-cirlce-widget w_bg_green">
                <span><i class="ico-user"></i></span>
            </a>
            <div class="w-meta-info">
                <span class="w-meta-value number-animate" data-value="{{ $aktifPegawai }}" data-animation-duration="1500">{{ $aktifPegawai }}</span>
                <span class="w-meta-title">Orang</span>
            </div>
        </div>
    </div>
    <div class="col-md-3 col-sm-6">
        <div class="iconic-w-wrap iconic-w-wrap">
            <span class="stat-w-title">Pegawai Tidak Aktif</span>
            <a href="{{ route('pekerja.index') }}" class="ico-cirlce-widget w_bg_blue_grey">
                <span><i class="fa fa-user-times"></i></span>
            </a>
            <div class="w-meta-info">
                <span class="w-meta-value number-animate" data-value="{{ $nonAktifPegawai }}" data-animation-duration="1500">{{ $nonAktifPegawai }}</span>
                <span class="w-meta-title">Orang</span>
            </div>
        </div>
    </div>

    <div class="col-md-3 col-sm-6">
        <div class="iconic-w-wrap iconic-w-wrap">
            <span class="stat-w-title">Pegawai Cuti</span>
            <a href="{{ route('pekerja.index') }}" class="ico-cirlce-widget w_bg_red">
                <span><i class="fa fa-street-view"></i></span>
            </a>
            <div class="w-meta-info">
                <span class="w-meta-value number-animate" data-value="{{ $cuti }}" data-animation-duration="1500">{{ $cuti }}</span>
                <span class="w-meta-title">Orang</span>
            </div>
        </div>
    </div>
</div>

<div class="row">
<div class="col-md-12">
        <div class="box-widget widget-module">
            <div class="widget-head clearfix">
                <span class="h-icon"><i class="fa fa-dashboard"></i></span>
                <h4>Selamat Datang</h4>
                <ul class="widget-action-bar pull-right">
                    <li><span class="waves-effect w-reload"><i class="fa fa-spinner"></i></span>
                    </li>
                </ul>
            </div>
            <div class="widget-container">
                <div class="table-responsive">
                    <table class="table w-order-list table-striped">
                        <thead>
                        <tr>
                            <th>
                                Selamat datang di {{ $title }} Manager.<br><br><br>
                            </th>
                            

                        </tr>
                        </thead>
                        
                    </table>
                </div>
            </div>
        </div>
    </div>

</div>

@endsection